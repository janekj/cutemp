# QMidi include file for QMake
CONFIG += c++11

INCLUDEPATH += $$PWD

SOURCES += $$PWD/sources/QMidiOut.cpp \
        $$PWD/sources/QMidiFile.cpp \
        $$PWD/sources/QMidiIn.cpp

HEADERS += $$PWD/headers/QMidiOut.h \
        $$PWD/headers/QMidiFile.h \
        $$PWD/headers/QMidiIn.h

win32 {
	LIBS += -lwinmm
        SOURCES += $$PWD/sources/OS/QMidi_Win32.cpp
}

linux* {
	LIBS += -lasound
        SOURCES += $$PWD/sources/OS/QMidi_ALSA.cpp
        HEADERS += $$PWD/sources/OS/QMidi_ALSA.h
}

haiku* {
	LIBS += -lmidi2
        SOURCES += $$PWD/sources/OS/QMidi_Haiku.cpp
        HEADERS += $$PWD/sources/OS/QMidi_Haiku.h
}

macx* {
	LIBS += -framework CoreMIDI -framework CoreFoundation -framework CoreAudio
        SOURCES += $$PWD/sources/OS/QMidi_CoreMidi.cpp
}

