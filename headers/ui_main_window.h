/********************************************************************************
** Form generated from reading UI file 'main_windowJoTfmu.ui'
**
** Created by: Qt User Interface Compiler version 5.15.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef MAIN_WINDOWJOTFMU_H
#define MAIN_WINDOWJOTFMU_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QWidget>
#include <QStyle>
#include <QStringList>
#include <QList>
#include <QFont>
#include <cmath>
#include <iostream>

QT_BEGIN_NAMESPACE
class ChannelUI: public QGridLayout {
    Q_OBJECT;

public:
    QProgressBar *channelsoundBAR;
    QSlider *channelvolumeVS;
    QLabel *channelnameLBL;
    QLabel *channelnumberLBL;
    QPushButton *muteBTN;
    QPushButton *soloBTN;
    QComboBox *comboBox;
    int channelNumber;

    ChannelUI(): QGridLayout() {
        channelsoundBAR = nullptr;
        channelvolumeVS = nullptr;
        channelnameLBL = nullptr;
        channelnumberLBL = nullptr;
        muteBTN = nullptr;
        soloBTN = nullptr;
        comboBox = nullptr;
        channelNumber = -1;
    };

    virtual ~ChannelUI() {
        if (channelsoundBAR != nullptr)
        {
            delete channelsoundBAR;
        }
        if (channelvolumeVS != nullptr)
        {
            delete channelvolumeVS;
        }
        if (channelnameLBL != nullptr)
        {
            delete channelnameLBL;
        }
        if (muteBTN != nullptr)
        {
            delete muteBTN;
        }
        if (soloBTN != nullptr)
        {
            delete soloBTN;
        }
        if (comboBox != nullptr)
        {
            delete comboBox;
        }
    };

    ChannelUI(const ChannelUI &other): QGridLayout(){
        channelNumber = other.channelNumber;
        channelsoundBAR = new QProgressBar(other.channelsoundBAR);
        this->addWidget(channelsoundBAR, 3, 0, 1, 1, Qt::AlignHCenter);

        channelvolumeVS = new QSlider(other.channelvolumeVS);
        this->addWidget(channelvolumeVS, 3, 1, 1, 1, Qt::AlignHCenter);

        channelnameLBL = new QLabel(other.channelnameLBL);
        this->addWidget(channelnameLBL, 0, 1, 1, 1, Qt::AlignRight);

        channelnumberLBL = new QLabel(other.channelnumberLBL);
        this->addWidget(channelnumberLBL, 0, 0, 1, 1, Qt::AlignLeft);

        muteBTN = new QPushButton(other.muteBTN);
        this->addWidget(muteBTN, 2, 0, 1, 1, Qt::AlignHCenter);

        soloBTN = new QPushButton(other.soloBTN);
        this->addWidget(soloBTN, 2, 1, 1, 1, Qt::AlignHCenter);

        comboBox = new QComboBox(other.comboBox);
        this->addWidget(comboBox, 1, 0, 1, 2);

        QObject::connect(channelvolumeVS, &QSlider::valueChanged,
                         this, &ChannelUI::slotVolumeChanged);
        QObject::connect(muteBTN, &QPushButton::clicked,
                         this, &ChannelUI::slotMuted);
        QObject::connect(soloBTN, &QPushButton::clicked,
                         this, &ChannelUI::slotSolo);


    };

    ChannelUI &operator=(const ChannelUI &other) {
        channelNumber = other.channelNumber;
        channelsoundBAR = other.channelsoundBAR;
        this->addWidget(channelsoundBAR, 3, 0, 1, 1, Qt::AlignHCenter);

        channelvolumeVS = other.channelvolumeVS;
        this->addWidget(channelvolumeVS, 3, 1, 1, 1, Qt::AlignHCenter);

        channelnameLBL = other.channelnameLBL;
        this->addWidget(channelnameLBL, 0, 1, 1, 1, Qt::AlignRight);

        channelnumberLBL = other.channelnumberLBL;
        this->addWidget(channelnumberLBL, 0, 0, 1, 1, Qt::AlignLeft);

        muteBTN = other.muteBTN;
        this->addWidget(muteBTN, 2, 0, 1, 1, Qt::AlignHCenter);

        soloBTN = other.soloBTN;
        this->addWidget(soloBTN, 2, 1, 1, 1, Qt::AlignHCenter);

        comboBox = other.comboBox;
        this->addWidget(comboBox, 1, 0, 1, 2);
        QObject::connect(channelvolumeVS, &QSlider::valueChanged,
                         this, &ChannelUI::slotVolumeChanged);
        QObject::connect(muteBTN, &QPushButton::clicked,
                         this, &ChannelUI::slotMuted);
        QObject::connect(soloBTN, &QPushButton::clicked,
                         this, &ChannelUI::slotSolo);


        return *this;
    };

    ChannelUI(int chno, QWidget *parent = nullptr): QGridLayout(parent){

        channelNumber = chno;
        channelsoundBAR = new QProgressBar(parent);
        channelsoundBAR->setObjectName(QString::fromUtf8("channelsoundBAR") + QString(chno));
        channelsoundBAR->setMaximum(100);
        channelsoundBAR->setValue(0);
        channelsoundBAR->setOrientation(Qt::Vertical);
        channelsoundBAR->setMinimumWidth(30);

        this->addWidget(channelsoundBAR, 3, 0, 1, 1, Qt::AlignHCenter);

        channelvolumeVS = new QSlider(parent);
        channelvolumeVS->setObjectName(QString::fromUtf8("channelvolumeVS") + QString(chno));
        channelvolumeVS->setAutoFillBackground(false);
        channelvolumeVS->setMaximum(127);
        channelvolumeVS->setValue(100);
        channelvolumeVS->setOrientation(Qt::Vertical);
        channelvolumeVS->setToolTip(QStringLiteral("Channel: %1\nVolume: %2").arg(channelNumber).arg(100));
        channelvolumeVS->setMinimumWidth(channelsoundBAR->minimumWidth());

        this->addWidget(channelvolumeVS, 3, 1, 1, 1, Qt::AlignHCenter);

        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Minimum);

        channelnameLBL = new QLabel(parent);
        channelnameLBL->setObjectName(QString::fromUtf8("channelnameLBL"));
        channelnameLBL->setSizePolicy(sizePolicy3);
        channelnameLBL->setMinimumWidth(channelsoundBAR->minimumWidth());

        this->addWidget(channelnameLBL, 0, 1, 1, 1, Qt::AlignRight);

        channelnumberLBL = new QLabel(parent);
        channelnumberLBL->setObjectName(QString::fromUtf8("channelnumberLBL"));
        channelnumberLBL->setText(QString::number(channelNumber));
        QFont boldfont;
        boldfont.setBold(true);
        channelnumberLBL->setFont(boldfont);
        channelnumberLBL->setSizePolicy(sizePolicy3);
        channelnumberLBL->setMinimumWidth(channelsoundBAR->minimumWidth());

        this->addWidget(channelnumberLBL, 0, 0, 1, 1, Qt::AlignLeft);

        muteBTN = new QPushButton(parent);
        muteBTN->setObjectName(QString::fromUtf8("muteBTN"));
        muteBTN->setText(QString("M"));
        muteBTN->setEnabled(true);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHeightForWidth(muteBTN->sizePolicy().hasHeightForWidth());
        muteBTN->setSizePolicy(sizePolicy);
        muteBTN->setMinimumWidth(channelsoundBAR->minimumWidth());
        muteBTN->setCheckable(true);

        this->addWidget(muteBTN, 2, 0, 1, 1, Qt::AlignHCenter);

        soloBTN = new QPushButton(parent);
        soloBTN->setObjectName(QString::fromUtf8("soloBTN"));
        soloBTN->setText(QString("S"));
        soloBTN->setEnabled(true);
        // sizePolicy.setHeightForWidth(soloBTN->sizePolicy().hasHeightForWidth());
        soloBTN->setSizePolicy(sizePolicy);
        soloBTN->setMinimumWidth(channelsoundBAR->minimumWidth());
        soloBTN->setCheckable(true);

        this->addWidget(soloBTN, 2, 1, 1, 1, Qt::AlignHCenter);

        comboBox = new QComboBox(parent);
        comboBox->addItems(QStringList(QList<QString>() = {
                "Acoustic Grand Piano",
                "Bright Acoustic Piano",
                "Electric Grand Piano",
                "Honky-tonk Piano",
                "Electric Piano 1 (Rhodes Piano)",
                "Electric Piano 2 (Chorused Piano)",
                "Harpsichord",
                "Clavinet",
                "Celesta",
                "Glockenspiel",
                "Music Box",
                "Vibraphone",
                "Marimba",
                "Xylophone",
                "Tubular Bells",
                "Dulcimer (Santur)",
                "Drawbar Organ (Hammond)",
                "Percussive Organ",
                "Rock Organ",
                "Church Organ",
                "Reed Organ",
                "Accordion (French)",
                "Harmonica",
                "Tango Accordion (Band neon)",
                "Acoustic Guitar (nylon)",
                "Acoustic Guitar (steel)",
                "Electric Guitar (jazz)",
                "Electric Guitar (clean)",
                "Electric Guitar (muted)",
                "Overdriven Guitar",
                "Distortion Guitar",
                "Guitar harmonics",
                "Acoustic Bass",
                "Electric Bass (fingered)",
                "Electric Bass (picked)",
                "Fretless Bass",
                "Slap Bass 1",
                "Slap Bass 2",
                "Synth Bass 1",
                "Synth Bass 2",
                "Violin",
                "Viola",
                "Cello",
                "Contrabass",
                "Tremolo Strings",
                "Pizzicato Strings",
                "Orchestral Harp",
                "Timpani",
                "String Ensemble 1 (strings)",
                "String Ensemble 2 (slow strings)",
                "SynthStrings 1",
                "SynthStrings 2",
                "Choir Aahs",
                "Voice Oohs",
                "Synth Voice",
                "Orchestra Hit",
                "Trumpet",
                "Trombone",
                "Tuba",
                "Muted Trumpet",
                "French Horn",
                "Brass Section",
                "SynthBrass 1",
                "SynthBrass 2",
                "Soprano Sax",
                "Alto Sax",
                "Tenor Sax",
                "Baritone Sax",
                "Oboe",
                "English Horn",
                "Bassoon",
                "Clarinet",
                "Piccolo",
                "Flute",
                "Recorder",
                "Pan Flute",
                "Blown Bottle",
                "Shakuhachi",
                "Whistle",
                "Ocarina",
                "Lead 1 (square wave)",
                "Lead 2 (sawtooth wave)",
                "Lead 3 (calliope)",
                "Lead 4 (chiffer)",
                "Lead 5 (charang)",
                "Lead 6 (voice solo)",
                "Lead 7 (fifths)",
                "Lead 8 (bass + lead)",
                "Pad 1 (new age Fantasia)",
                "Pad 2 (warm)",
                "Pad 3 (polysynth)",
                "Pad 4 (choir space voice)",
                "Pad 5 (bowed glass)",
                "Pad 6 (metallic pro)",
                "Pad 7 (halo)",
                "Pad 8 (sweep)",
                "FX 1 (rain)",
                "FX 2 (soundtrack)",
                "FX 3 (crystal)",
                "FX 4 (atmosphere)",
                "FX 5 (brightness)",
                "FX 6 (goblins)",
                "FX 7 (echoes, drops)",
                "FX 8 (sci-fi, star theme)",
                "Sitar",
                "Banjo",
                "Shamisen",
                "Koto",
                "Kalimba",
                "Bag pipe",
                "Fiddle",
                "Shanai",
                "Tinkle Bell",
                "Agogo",
                "Steel Drums",
                "Woodblock",
                "Taiko Drum",
                "Melodic Tom",
                "Synth Drum",
                "Reverse Cymbal",
                "Guitar Fret Noise",
                "Breath Noise",
                "Seashore",
                "Bird Tweet",
                "Telephone Ring",
                "Helicopter",
                "Applause",
                "Gunshot"
        }));
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setMinimumWidth(10);
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy2.setHeightForWidth(comboBox->sizePolicy().hasHeightForWidth());
        comboBox->setSizePolicy(sizePolicy2);
        comboBox->setToolTip(comboBox->currentText());

        this->addWidget(comboBox, 1, 0, 1, 2);
        this->setColumnStretch(0, 1);
        this->setColumnStretch(1, 1);
        this->setColumnMinimumWidth(0, channelsoundBAR->minimumWidth());
        this->setColumnMinimumWidth(1, channelsoundBAR->minimumWidth());

        QObject::connect(channelvolumeVS, &QSlider::valueChanged,
                         this, &ChannelUI::slotVolumeChanged);
        QObject::connect(muteBTN, &QPushButton::clicked,
                         this, &ChannelUI::slotMuted);
        QObject::connect(soloBTN, &QPushButton::clicked,
                         this, &ChannelUI::slotSolo);
        QObject::connect(comboBox, QOverload<int>::of(&QComboBox::currentIndexChanged),
                         this, &ChannelUI::slotProgramChanged);

    };

    void setVisible(bool visible = true)
    {
        channelsoundBAR->setVisible(visible);
        channelvolumeVS->setVisible(visible);
        channelnameLBL->setVisible(visible);
        channelnumberLBL->setVisible(visible);
        muteBTN->setVisible(visible);
        soloBTN->setVisible(visible);
        comboBox->setVisible(visible);
        this->setEnabled(visible);
        this->setColumnMinimumWidth(0, visible?channelsoundBAR->minimumWidth():0);
        this->setColumnMinimumWidth(1, visible?channelsoundBAR->minimumWidth():0);
    }

    void changeName(QString name)
    {
        channelnameLBL->setText(name);
        channelnameLBL->setToolTip(channelnumberLBL->text() + QString(": ") + name);
    }

signals:
    void signalVolumeChanged(int channel, int volume);
    void signalProgramChanged(int channel, int instrument);
    void signalChannelMuted(int channel);
    void signalChannelSolo(int channel);

public slots:
    void slotNoteEvent(int velocity, bool on)
    {
        if (on)
        {
            channelsoundBAR->setValue((int) round((double) velocity * channelvolumeVS->value() / 127 / 127 * 100));
        }
        else
        {
            channelsoundBAR->setValue(0);
        }
    }
    void slotVolumeChanged(int value)
    {
        emit signalVolumeChanged(channelNumber, value);
        channelvolumeVS->setToolTip(QStringLiteral("Channel: %1\nVolume: %2").arg(channelNumber).arg(value));
    }
    void slotMuted()
    {
        emit signalChannelMuted(channelNumber);
    }
    void slotSolo()
    {
        emit signalChannelSolo(channelNumber);
    }
    void slotProgramChanged(int value)
    {
        emit signalProgramChanged(channelNumber, value);
        comboBox->setToolTip(comboBox->currentText());
    }

};

class Ui_MainWindow
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionSaveAsText;
    QAction *actionMIDI_Output;
    QAction *actionChannels;
    QAction *actionAbout;
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QVBoxLayout *uppermostVL;
    QHBoxLayout *channelsHL;
    // channels
    QList<ChannelUI*> channelList;
    //end channels
    QVBoxLayout *playbackVL;
    QHBoxLayout *tempovolumeHL;
    QLabel *tempoLBL;
    QSlider *tempoSL;
    QPushButton *resettempoBTN;
//    QLabel *volumeLBL;
//    QSlider *volumeHL;
    QPushButton *resetvolumeBTN;
    QHBoxLayout *timelineHL;
    QPushButton *bwdBTN;
    QPushButton *playpauseBTN;
    QPushButton *fwdBTN;
    QPushButton *stopBTN;
    QSlider *timelineSL;
    QLabel *timelineLBL;
    QMenuBar *menubar;
    QMenu *menuFile;
    QMenu *menuMIDI_Output;
    QMenu *menuHelp;
    QStatusBar *statusbar;
    QLabel *statusLeft;
    QLabel *statusMiddle;
    QLabel *statusRight;
    //QScrollArea *scrollchannels;
    //QWidget *channelsWGT;

    void setupUi(QMainWindow *MainWindow, int theme = 0)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(747, 556);
        MainWindow->setWindowIcon(QIcon("../icons/program_icon.svg"));
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        actionSaveAsText = new QAction(MainWindow);
        actionSaveAsText->setObjectName(QString::fromUtf8("actionSaveAsText"));
        actionMIDI_Output = new QAction(MainWindow);
        actionMIDI_Output->setObjectName(QString::fromUtf8("actionMIDI_Output"));
        actionChannels = new QAction(MainWindow);
        actionChannels->setObjectName(QString::fromUtf8("actionChannels"));
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        uppermostVL = new QVBoxLayout();
        uppermostVL->setObjectName(QString::fromUtf8("uppermostVL"));

        //scrollchannels = new QScrollArea();
        //scrollchannels->setObjectName(QString::fromUtf8("scrollchannels"));
        //channelsWGT = new QWidget();
        //channelsHL = new QHBoxLayout(channelsWGT);
        channelsHL = new QHBoxLayout();
        channelsHL->setObjectName(QString::fromUtf8("channelsHL"));

        for (int i = 0; i < 16; i++)
        {
            channelList.append(new ChannelUI(i));
            channelsHL->addLayout(channelList.at(i));
            channelsHL->setStretch(i, 1);
        }

        //uppermostVL->addWidget(scrollchannels);
        uppermostVL->addLayout(channelsHL);
        //scrollchannels->setWidget(channelsWGT);

        playbackVL = new QVBoxLayout();
        playbackVL->setObjectName(QString::fromUtf8("playbackVL"));
        tempovolumeHL = new QHBoxLayout();
        tempovolumeHL->setObjectName(QString::fromUtf8("tempovolumeHL"));
        tempoLBL = new QLabel(centralwidget);
        tempoLBL->setObjectName(QString::fromUtf8("tempoLBL"));

        tempovolumeHL->addWidget(tempoLBL);

        tempoSL = new QSlider(centralwidget);
        tempoSL->setObjectName(QString::fromUtf8("tempoSL"));
        tempoSL->setMinimum(10);
        tempoSL->setMaximum(300);
        tempoSL->setValue(100);
        tempoSL->setToolTip(QString::number(100) + QString(" %"));
        tempoSL->setOrientation(Qt::Horizontal);

        tempovolumeHL->addWidget(tempoSL);

        resettempoBTN = new QPushButton(centralwidget);
        resettempoBTN->setObjectName(QString::fromUtf8("resettempoBTN"));

        tempovolumeHL->addWidget(resettempoBTN);

//        volumeLBL = new QLabel(centralwidget);
//        volumeLBL->setObjectName(QString::fromUtf8("volumeLBL"));

//        tempovolumeHL->addWidget(volumeLBL);

//        volumeHL = new QSlider(centralwidget);
//        volumeHL->setObjectName(QString::fromUtf8("volumeHL"));
//        volumeHL->setMaximum(120);
//        volumeHL->setValue(100);
//        volumeHL->setOrientation(Qt::Horizontal);

//        tempovolumeHL->addWidget(volumeHL);

//        resetvolumeBTN = new QPushButton(centralwidget);
//        resetvolumeBTN->setObjectName(QString::fromUtf8("resetvolumeBTN"));

//        tempovolumeHL->addWidget(resetvolumeBTN);


        playbackVL->addLayout(tempovolumeHL);

        timelineHL = new QHBoxLayout();
        timelineHL->setObjectName(QString::fromUtf8("timelineHL"));
        bwdBTN = new QPushButton(centralwidget);
        bwdBTN->setObjectName(QString::fromUtf8("bwdBTN"));
        bwdBTN->setShortcut(QKeySequence::MoveToPreviousChar);

        timelineHL->addWidget(bwdBTN);

        playpauseBTN = new QPushButton(centralwidget);
        playpauseBTN->setObjectName(QString::fromUtf8("playpauseBTN"));
        playpauseBTN->setShortcut(Qt::Key_Space);
        playpauseBTN->setShortcut(QKeySequence("Space"));

        timelineHL->addWidget(playpauseBTN);

        fwdBTN = new QPushButton(centralwidget);
        fwdBTN->setObjectName(QString::fromUtf8("fwdBTN"));
        fwdBTN->setShortcut(QKeySequence::MoveToNextChar);

        timelineHL->addWidget(fwdBTN);

        stopBTN = new QPushButton(centralwidget);
        stopBTN->setObjectName(QString::fromUtf8("stopBTN"));
        stopBTN->setShortcut(QKeySequence::Cancel);

        timelineHL->addWidget(stopBTN);

        timelineSL = new QSlider(centralwidget);
        timelineSL->setObjectName(QString::fromUtf8("timelineSL"));
        timelineSL->setOrientation(Qt::Horizontal);
        timelineSL->setMaximum(1000);

        timelineHL->addWidget(timelineSL);

        timelineLBL = new QLabel(centralwidget);
        timelineLBL->setObjectName(QString::fromUtf8("timelineLBL"));

        timelineHL->addWidget(timelineLBL);

        playbackVL->addLayout(timelineHL);

        uppermostVL->addLayout(playbackVL);

        gridLayout->addLayout(uppermostVL, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 747, 29));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuMIDI_Output = new QMenu(menubar);
        menuMIDI_Output->setObjectName(QString::fromUtf8("menuMIDI_Output"));
        menuHelp = new QMenu(menubar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        statusLeft = new QLabel(MainWindow);
        statusLeft->setMargin(5);
        statusLeft->setText("No MIDI output connected!");
        //statusLeft->setFrameStyle(QFrame::Panel | QFrame::Sunken);
        statusMiddle = new QLabel(MainWindow);
        statusMiddle->setMargin(5);
        statusMiddle->setAlignment(Qt::Alignment(Qt::AlignmentFlag::AlignCenter));
        statusMiddle->setText("No file loaded!");
        //statusMiddle->setFrameStyle(QFrame::Panel | QFrame::Sunken);
        statusRight = new QLabel(MainWindow);
        statusRight->setMargin(5);
        statusRight->setAlignment(Qt::Alignment(Qt::AlignmentFlag::AlignRight));
        //statusRight->setFrameStyle(QFrame::Panel | QFrame::Sunken);
        statusbar->addPermanentWidget(statusLeft, 1);
        statusbar->addPermanentWidget(statusMiddle, 1);
        statusbar->addPermanentWidget(statusRight, 1);

        menubar->addAction(menuFile->menuAction());
        menubar->addAction(menuMIDI_Output->menuAction());
        menubar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave);
        menuFile->addAction(actionSaveAsText);
        menuMIDI_Output->addAction(actionMIDI_Output);
        menuMIDI_Output->addAction(actionChannels);
        menuHelp->addAction(actionAbout);

        switch (theme)
        {
        case 1: // black icons
            stopBTN->setIcon(QIcon("../icons/black/stop.svg"));
            playpauseBTN->setIcon(QIcon("../icons/black/play.svg"));
            bwdBTN->setIcon(QIcon("../icons/black/backward.svg"));
            fwdBTN->setIcon(QIcon("../icons/black/forward.svg"));
            break;
        case 2: // white icons
            stopBTN->setIcon(QIcon("../icons/white/stop.svg"));
            playpauseBTN->setIcon(QIcon("../icons/white/play.svg"));
            bwdBTN->setIcon(QIcon("../icons/white/backward.svg"));
            fwdBTN->setIcon(QIcon("../icons/white/forward.svg"));
            break;
        default: // default Qt icons
            bwdBTN->setIcon(MainWindow->style()->standardIcon(QStyle::SP_MediaSeekBackward));
            stopBTN->setIcon(MainWindow->style()->standardIcon(QStyle::SP_MediaStop));
            fwdBTN->setIcon(MainWindow->style()->standardIcon(QStyle::SP_MediaSeekForward));
            playpauseBTN->setIcon(MainWindow->style()->standardIcon(QStyle::SP_MediaPlay));
            break;
        }


        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "Cute Midi Player", nullptr));
        actionOpen->setText(QCoreApplication::translate("MainWindow", "Open", nullptr));
#if QT_CONFIG(tooltip)
        actionOpen->setToolTip(QCoreApplication::translate("MainWindow", "Browse for .midi file", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        actionOpen->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+O", nullptr));
#endif // QT_CONFIG(shortcut)
        actionSave->setText(QCoreApplication::translate("MainWindow", "Save", nullptr));
#if QT_CONFIG(tooltip)
        actionSave->setToolTip(QCoreApplication::translate("MainWindow", "Save .midi file with defined channel levels", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        actionSave->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+S", nullptr));
#endif // QT_CONFIG(shortcut)
        actionSaveAsText->setText(QCoreApplication::translate("MainWindow", "Save as text", nullptr));
#if QT_CONFIG(tooltip)
        actionSaveAsText->setToolTip(QCoreApplication::translate("MainWindow", "Save .midi file in human readable form", nullptr));
#endif // QT_CONFIG(tooltip)
        actionMIDI_Output->setText(QCoreApplication::translate("MainWindow", "MIDI output", nullptr));
        actionChannels->setText(QCoreApplication::translate("MainWindow", "Channels", nullptr));
        actionAbout->setText(QCoreApplication::translate("MainWindow", "About", nullptr));

        tempoLBL->setText(QCoreApplication::translate("MainWindow", "Tempo", nullptr));
#if QT_CONFIG(tooltip)
        resettempoBTN->setToolTip(QCoreApplication::translate("MainWindow", "Reset tempo to default value saved in .midi file", nullptr));
#endif // QT_CONFIG(tooltip)
        resettempoBTN->setText(QCoreApplication::translate("MainWindow", "Reset Tempo", nullptr));

       //volumeLBL->setText(QCoreApplication::translate("MainWindow", "Volume", nullptr));
#if QT_CONFIG(tooltip)
       // resetvolumeBTN->setToolTip(QCoreApplication::translate("MainWindow", "Reset volume to 100 %", nullptr));
#endif // QT_CONFIG(tooltip)
       // resetvolumeBTN->setText(QCoreApplication::translate("MainWindow", "Reset Volume", nullptr));

#if QT_CONFIG(tooltip)
        bwdBTN->setToolTip(QCoreApplication::translate("MainWindow", "One bar backwards", nullptr));
#endif // QT_CONFIG(tooltip)
        //bwdBTN->setText(QCoreApplication::translate("MainWindow", "Bwd", nullptr));
#if QT_CONFIG(tooltip)
        playpauseBTN->setToolTip(QCoreApplication::translate("MainWindow", "Play .midi file", nullptr));
#endif // QT_CONFIG(tooltip)
        // playpauseBTN->setText(QCoreApplication::translate("MainWindow", "Play", nullptr));
#if QT_CONFIG(tooltip)
        fwdBTN->setToolTip(QCoreApplication::translate("MainWindow", "One bar forward", nullptr));
#endif // QT_CONFIG(tooltip)
       // fwdBTN->setText(QCoreApplication::translate("MainWindow", "Fwd", nullptr));
#if QT_CONFIG(tooltip)
        stopBTN->setToolTip(QCoreApplication::translate("MainWindow", "Stop playing and return to the beginning of the song", nullptr));
#endif // QT_CONFIG(tooltip)
        // stopBTN->setText(QCoreApplication::translate("MainWindow", "Stop", nullptr));
        timelineLBL->setText(QCoreApplication::translate("MainWindow", "0:00/0:00", nullptr));
        menuFile->setTitle(QCoreApplication::translate("MainWindow", "File", nullptr));
#if QT_CONFIG(tooltip)
        menuMIDI_Output->setToolTip(QCoreApplication::translate("MainWindow", "Select MIDI output device (port)", nullptr));
#endif // QT_CONFIG(tooltip)
        menuMIDI_Output->setTitle(QCoreApplication::translate("MainWindow", "Edit", nullptr));
        menuHelp->setTitle(QCoreApplication::translate("MainWindow", "Help", nullptr));
    } // retranslateUi

};

namespace Ui {
class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // MAIN_WINDOWJOTFMU_H
