#ifndef ABOUTWINDOW_H
#define ABOUTWINDOW_H

#include <QDialog>
#include "headers/ui_about.h"

QT_BEGIN_NAMESPACE
namespace Ui4 { class AboutWindow; }
QT_END_NAMESPACE

class AboutWindow : public QDialog
{
    Q_OBJECT
public:
    explicit AboutWindow(QWidget *parent = nullptr);
    ~AboutWindow();

private:
    Ui4::AboutWindow *ui;

};

#endif // ABOUTWINDOW_H
