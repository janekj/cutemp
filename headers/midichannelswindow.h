#ifndef MIDICHANNELSWINDOW_H
#define MIDICHANNELSWINDOW_H

#include <QObject>
#include <QDialog>
#include "headers/ui_main_window.h"

QT_BEGIN_NAMESPACE
namespace Ui3 { class MidiChannelsWindow; }
QT_END_NAMESPACE

class MidiChannelsWindow : public QDialog
{
    Q_OBJECT

public:
    MidiChannelsWindow(Ui::MainWindow *ui, QWidget *parent = nullptr);
    ~MidiChannelsWindow();
    void setValues();
    void getValues() const;

public slots:
    // void slotname...
    void slotAccept();
    void slotCancell();


signals:
    void acceptSelection(QString midikey);

private:
    Ui3::MidiChannelsWindow *ui;
    Ui::MainWindow *main_ui;
};

#endif // MIDICHANNELSWINDOW_H
