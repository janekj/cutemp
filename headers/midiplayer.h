#ifndef MIDIPLAYER_H
#define MIDIPLAYER_H

#include <QThread>
#include "QMidi/headers/QMidiFile.h"
#include "QMidi/headers/QMidiOut.h"
#include <QTimeLine>
#include <QSet>
#include <fstream>
#include "headers/ui_main_window.h"

#define OTHERSLEVEL 0.5 // level of other than solo channels

class MidiPlayer : public QThread
{
    Q_OBJECT
public:
    MidiPlayer();
    MidiPlayer(QMidiFile* file, QMidiOut* out);
    ~MidiPlayer();
    void play();
    void pause();
    void stop(bool blocktimeupdate = false);
    void setMidiFile(QMidiFile *file);
    void setMidiOut(QMidiOut *midioutput);
    bool isPlaying();
    /// print text version of the midifile
    int printReadable(Ui_MainWindow *ui, QString filename);
    /// Total song duration (including tempo factor) in ms
    double getTotalDuration() const;
    /// Original song duration (not including tempo factor) in ms
    int getOrigTotalDuration() const;
    /// Prepared for playing (both midifile and midiout are notnull)
    bool prepared() const;
    /// toggle solo for channel
    void soloChannel(int channel);
    /// toggle mute for channel
    void muteChannel(int channel);
    /// reset all channel controllers
    void resetControllers(QList<ChannelUI*> channelList);
    /// add info from ui to midifile and save it
    void saveMidiFile(Ui_MainWindow *ui, QString filename);
    /// move no_bars bars forward (backward if no_bars < 0)
    void moveByBar(int no_bars);

public slots:
    void slotTimeUpdate(qreal curtime); // update due to time_line
    void slotChannelVolumeChanged(int channel, int volume);
    void slotChannelProgramChanged(int channel, int value);
    void slotTimeChanged(int promiles); // update due to  timeline slider (1–1000)
    void slotTempoFactorUpdate(int factor);
    void slotFinished();

signals:
    void signalTimeUpdated(qreal curtime, QPair<int, int> barBeat);
    void signalNoteOn(int channel, int velocity);
    void signalNoteOff(int channel, int velocity);
    void signalAllNotesOff();
    void signalStopped();
    void signalChannelVolume(int channel, int volume);
    void signalChannelProgram(int channel, int instrument); // instrument change
    void signalChannelName(int channel, QString name);
    void signalChannelVisibility(int channel, bool visible);
    void signalTimeChanged(int value, QPair<int, int> barBeat);

private:
    QMidiFile* midi_file;
    QMidiOut* midi_out;
    QTimeLine *time_line;
    /// original total time in ms
    int orig_total_duration;
    /// tempo factor given by the tempo slider
    double tempo_factor;
    /// next event waiting to be send by midiplayer
    int nextevent;
    /// time when the nextevent should come
    qint64 nexttime;
    /// set of currently muted channels
    QSet<int> mutedchannels;
    /// set of currently solo channels
    QSet<int> solochannels;
    /// set of channels with note events
    QSet<int> activechannels;
    /// channel names
    QString channelnames[16];

    /// Events preprocessing
    /// Channel volume and Program changes are applied once and deleted from the MidiFile.
    /// Active channels (with note events) are found and saved.
    /// Channel names (if present are retrieved).
    void preprocessEvents();

    /// Events processing before save
    /// Channel volume, Program changes and Channel names are added to the MidiFile before saving
    void addEventsForSave();

protected:

};

#endif // MIDIPLAYER_H
