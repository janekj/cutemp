#ifndef MIDIOUTPUTWINDOW_H
#define MIDIOUTPUTWINDOW_H
#include <QtWidgets/QDialog>
#include "QMidi/headers/QMidiOut.h"
#include "headers/mainwindow.h"

QT_BEGIN_NAMESPACE
namespace Ui2 { class MidiOutputWindow; }
QT_END_NAMESPACE

class MidiOutputWindow : public QDialog
{
    Q_OBJECT

public:
    MidiOutputWindow(QWidget *parent = nullptr);
    ~MidiOutputWindow();


public slots:
    // void slotname...
    void slotAccept();
    void slotCancell();
    void slotTest();

signals:
    void acceptSelection(QString midikey);

private:
    Ui2::MidiOutputWindow *ui;
};
#endif // MIDIOUTPUTWINDOW_H
