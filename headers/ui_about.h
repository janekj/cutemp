/********************************************************************************
** Form generated from reading UI file 'aboutabTKsq.ui'
**
** Created by: Qt User Interface Compiler version 5.15.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef ABOUTABTKSQ_H
#define ABOUTABTKSQ_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QDialog>

QT_BEGIN_NAMESPACE

class Ui_AboutWindow
{
public:
    QLabel *label;
    QWidget *mainWDG;

    ~Ui_AboutWindow()
    {
        if (label != nullptr)
        {
            delete label;
        }
    }

    void setupUi(QDialog *Form)
    {
        if (Form->objectName().isEmpty())
            Form->setObjectName(QString::fromUtf8("Form"));
        Form->resize(402, 472);
        mainWDG = new QWidget(Form);
        label = new QLabel(mainWDG);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 10, 381, 451));
        QSizePolicy sizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);
        label->setAlignment(Qt::AlignCenter);
        label->setWordWrap(true);

        retranslateUi(Form);
        mainWDG->updateGeometry();
        mainWDG->adjustSize();
        Form->setMinimumSize(mainWDG->size());

        QMetaObject::connectSlotsByName(Form);
    } // setupUi

    void retranslateUi(QDialog *Form)
    {
        Form->setWindowTitle(QCoreApplication::translate("Form", "About CuteMIDIPlayer", nullptr));
        label->setText(QCoreApplication::translate("Form", "<html><head/><body><p><span style=\" font-weight:600;\">CuteMIDIPlayer</span></p><p align=\"center\"><img src=\":/icons/program_icon.svg\"/></p><p>Cross-platform MIDI player based on <a href=\"https://github.com/waddlesplash/QMidi\"><span style=\" text-decoration: underline; color:#0000ff;\">QMidi</span></a>.</p><p>Written in <a href=\"https://www.qt.io/\"><span style=\" text-decoration: underline; color:#0000ff;\">Qt</span></a></p><p>Change channel volumes and instruments and save the resulting MIDI file. Before playing, you must connect MIDI output (Edit\342\206\222MIDI output).</p><p>Created to simplify rehearsal and music editing.</p><p>Creator JJ (janekj2727(at)gmail.com).</p><p>Distributed under <a href=\"https://www.gnu.org/licenses/gpl-3.0.html\"><span style=\" text-decoration: underline; color:#0000ff;\">GPLv3</span></a>: (shortly) Anyone can use, modify and distribute the software for non-commercial purposes.</p></body></html>", nullptr));
    } // retranslateUi

};

namespace Ui4 {
    class AboutWindow: public Ui_AboutWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // ABOUTABTKSQ_H
