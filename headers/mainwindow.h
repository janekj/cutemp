#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include "headers/midiplayer.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr, char *file = nullptr, char *port = nullptr, int th = 0);
    ~MainWindow();


public slots:
    // void slotname...
    /// opens open file dialog and opens the selected file
    void slotOpenFile();
    /// opens save file dialog to select the name of the new file and then saves the file
    void slotSaveFile();
    /// opens window with reachable midi output devices to choose one
    void slotSelectMIDIOut();
    /// opens window with midi channels option (visibility, name, instrument)
    void slotMidiChannelsSetup();
    /// opens save file dialog to select the name of the text file and then saves the midifile text report
    void slotSaveFileAsText();
    /// open MIDI output
    void slotSetMidiOutput(QString midikey);
    /// open 'about' window
    void slotAbout();
    /// play/pause button
    void slotPlayPause();
    /// stop button
    void slotStop();
    /// forward button
    void slotFwd();
    /// backward button
    void slotBwd();
    void slotTimeUpdated(qreal curtime, QPair<int, int> barBeat);
    void slotNoteOn(int channel, int velocity);
    void slotNoteOff(int channel, int velocity);
    /// zero all sound bars showing the current sound
    void slotZeroSoundBars();
    /// mute/unmute channel
    void slotChannelMute(int channel);
    /// solo/unsolo channel
    void slotChannelSolo(int channel);
    /// reset tempo
    void slotTempoReset();
    /// change tempo (to change tooltip)
    void slotTempoChanged(int value);
    /// channel volume from midi source
    void slotChannelVolume(int channel, int volume);
    /// channel program from midi source
    void slotChannelProgram(int channel, int instrument);
    /// time change due to user slider move
    void slotTimeChanged(int value, QPair<int, int> barBeat);
    /// channel name
    void slotChannelName(int channel, QString name);
    /// channel visibility
    void slotChannelVisibility(int channel, bool visible);

signals:


private:
    Ui::MainWindow *ui;
    MidiPlayer *midiplayer;
    QString songduration;
    /// open file 'filename'
    void openFile(QString filename);
    /// convert ms (int) to mm:ss (QString)
    QString msToMMSS(int timeinms);
    /// disable buttons and sliders that should not be touched until midi file is loaded and midi output is chosen
    void disablePlaying(bool disable = true);
    /// actions before closing
    void closeEvent(QCloseEvent *event) override;
    /// theme (0: default, 1: black icons, 2: white icons
    int theme;
    /// try to set MIDI output (if the only one/if FluidSynth found)
    void setMidiOutputHeuristic();
};
#endif // MAINWINDOW_H
