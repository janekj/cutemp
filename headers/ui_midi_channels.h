/********************************************************************************
** Form generated from reading UI file 'midi_channelsSzEOQb.ui'
**
** Created by: Qt User Interface Compiler version 5.15.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef MIDI_CHANNELSSZEOQB_H
#define MIDI_CHANNELSSZEOQB_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE
class Ui_MidiChannel : public QHBoxLayout {
public:
    QLabel *chnumberLBL;
    QCheckBox *channelvisChB;
    QLineEdit *chnameLE;
    QComboBox *chinstrumentCB;

    Ui_MidiChannel(int channo, QWidget *parent = nullptr): QHBoxLayout(parent)
    {
        setObjectName(QString::fromUtf8("channelHL"));
        chnumberLBL = new QLabel();
        chnumberLBL->setObjectName(QString::fromUtf8("chnumberLBL"));
        chnumberLBL->setText(QString::number(channo));
        chnumberLBL->setMinimumWidth(120);

        addWidget(chnumberLBL, 1, Qt::AlignHCenter);

        channelvisChB = new QCheckBox();
        channelvisChB->setObjectName(QString::fromUtf8("channelvisChB"));
        channelvisChB->setMinimumWidth(120);

        addWidget(channelvisChB, 1, Qt::AlignHCenter);

        chnameLE = new QLineEdit();
        chnameLE->setObjectName(QString::fromUtf8("chnameLE"));

        addWidget(chnameLE, 1, Qt::AlignHCenter);

        chinstrumentCB = new QComboBox();
        chinstrumentCB->setObjectName(QString::fromUtf8("chinstrumentCB"));
        chinstrumentCB->addItems(QStringList(QList<QString>() = {
                "Acoustic Grand Piano",
                "Bright Acoustic Piano",
                "Electric Grand Piano",
                "Honky-tonk Piano",
                "Electric Piano 1 (Rhodes Piano)",
                "Electric Piano 2 (Chorused Piano)",
                "Harpsichord",
                "Clavinet",
                "Celesta",
                "Glockenspiel",
                "Music Box",
                "Vibraphone",
                "Marimba",
                "Xylophone",
                "Tubular Bells",
                "Dulcimer (Santur)",
                "Drawbar Organ (Hammond)",
                "Percussive Organ",
                "Rock Organ",
                "Church Organ",
                "Reed Organ",
                "Accordion (French)",
                "Harmonica",
                "Tango Accordion (Band neon)",
                "Acoustic Guitar (nylon)",
                "Acoustic Guitar (steel)",
                "Electric Guitar (jazz)",
                "Electric Guitar (clean)",
                "Electric Guitar (muted)",
                "Overdriven Guitar",
                "Distortion Guitar",
                "Guitar harmonics",
                "Acoustic Bass",
                "Electric Bass (fingered)",
                "Electric Bass (picked)",
                "Fretless Bass",
                "Slap Bass 1",
                "Slap Bass 2",
                "Synth Bass 1",
                "Synth Bass 2",
                "Violin",
                "Viola",
                "Cello",
                "Contrabass",
                "Tremolo Strings",
                "Pizzicato Strings",
                "Orchestral Harp",
                "Timpani",
                "String Ensemble 1 (strings)",
                "String Ensemble 2 (slow strings)",
                "SynthStrings 1",
                "SynthStrings 2",
                "Choir Aahs",
                "Voice Oohs",
                "Synth Voice",
                "Orchestra Hit",
                "Trumpet",
                "Trombone",
                "Tuba",
                "Muted Trumpet",
                "French Horn",
                "Brass Section",
                "SynthBrass 1",
                "SynthBrass 2",
                "Soprano Sax",
                "Alto Sax",
                "Tenor Sax",
                "Baritone Sax",
                "Oboe",
                "English Horn",
                "Bassoon",
                "Clarinet",
                "Piccolo",
                "Flute",
                "Recorder",
                "Pan Flute",
                "Blown Bottle",
                "Shakuhachi",
                "Whistle",
                "Ocarina",
                "Lead 1 (square wave)",
                "Lead 2 (sawtooth wave)",
                "Lead 3 (calliope)",
                "Lead 4 (chiffer)",
                "Lead 5 (charang)",
                "Lead 6 (voice solo)",
                "Lead 7 (fifths)",
                "Lead 8 (bass + lead)",
                "Pad 1 (new age Fantasia)",
                "Pad 2 (warm)",
                "Pad 3 (polysynth)",
                "Pad 4 (choir space voice)",
                "Pad 5 (bowed glass)",
                "Pad 6 (metallic pro)",
                "Pad 7 (halo)",
                "Pad 8 (sweep)",
                "FX 1 (rain)",
                "FX 2 (soundtrack)",
                "FX 3 (crystal)",
                "FX 4 (atmosphere)",
                "FX 5 (brightness)",
                "FX 6 (goblins)",
                "FX 7 (echoes, drops)",
                "FX 8 (sci-fi, star theme)",
                "Sitar",
                "Banjo",
                "Shamisen",
                "Koto",
                "Kalimba",
                "Bag pipe",
                "Fiddle",
                "Shanai",
                "Tinkle Bell",
                "Agogo",
                "Steel Drums",
                "Woodblock",
                "Taiko Drum",
                "Melodic Tom",
                "Synth Drum",
                "Reverse Cymbal",
                "Guitar Fret Noise",
                "Breath Noise",
                "Seashore",
                "Bird Tweet",
                "Telephone Ring",
                "Helicopter",
                "Applause",
                "Gunshot"
        }));

        addWidget(chinstrumentCB);

        setStretch(0, 1);
        setStretch(1, 1);
        setStretch(2, 2);
        setStretch(3, 2);
    }

    ~Ui_MidiChannel()
    {
        if (chnumberLBL != nullptr)
            delete chnumberLBL;
        if (channelvisChB != nullptr)
            delete channelvisChB;
        if (chnameLE != nullptr)
            delete chnameLE;
        if (chinstrumentCB != nullptr)
            delete chinstrumentCB;
    }
};

class Ui_ChannelsWindow
{
public:
    QWidget *mainWDG;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *labelsHL;
    QLabel *chNoLBL;
    QLabel *visibLBL;
    QLabel *chNameLBL;
    QLabel *instrumentLBL;
    QList<Ui_MidiChannel*> channelList;

    QDialogButtonBox *buttonBox;

    ~Ui_ChannelsWindow()
    {
        if (labelsHL != nullptr)
            delete labelsHL;
        for (int i = 0; i < 16; i++)
            delete channelList.at(i);

    }

    void setupUi(QDialog *ChannelsWindow)
    {
        if (ChannelsWindow->objectName().isEmpty())
        {
            ChannelsWindow->setObjectName(QString::fromUtf8("MIDIChannels"));
        }
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ChannelsWindow->sizePolicy().hasHeightForWidth());
        ChannelsWindow->setSizePolicy(sizePolicy);
        //ChannelsWindow->setMinimumSize(QSize(300, 200));
        mainWDG = new QWidget(ChannelsWindow);
        mainWDG->setObjectName(QString::fromUtf8("mainWDG"));
        verticalLayout = new QVBoxLayout(mainWDG);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(5, 5, 5, 5);
        labelsHL = new QHBoxLayout();
        labelsHL->setObjectName(QString::fromUtf8("labelsHL"));
        chNoLBL = new QLabel(mainWDG);
        chNoLBL->setObjectName(QString::fromUtf8("chNoLBL"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        chNoLBL->setFont(font);

        labelsHL->addWidget(chNoLBL, 0, Qt::AlignHCenter);

        visibLBL = new QLabel(mainWDG);
        visibLBL->setObjectName(QString::fromUtf8("visibLBL"));
        visibLBL->setFont(font);

        labelsHL->addWidget(visibLBL, 0, Qt::AlignHCenter);

        chNameLBL = new QLabel(mainWDG);
        chNameLBL->setObjectName(QString::fromUtf8("chNameLBL"));
        chNameLBL->setFont(font);

        labelsHL->addWidget(chNameLBL);

        instrumentLBL = new QLabel(mainWDG);
        instrumentLBL->setObjectName(QString::fromUtf8("instrumentLBL"));
        instrumentLBL->setFont(font);

        labelsHL->addWidget(instrumentLBL);

        labelsHL->setStretch(0, 1);
        labelsHL->setStretch(1, 1);
        labelsHL->setStretch(2, 2);
        labelsHL->setStretch(3, 2);

        verticalLayout->addLayout(labelsHL);

        // add channels
        for (int i = 0; i < 16; i++)
        {
            channelList.append(new Ui_MidiChannel(i));
            verticalLayout->addLayout(channelList.at(i));
        }

        buttonBox = new QDialogButtonBox(mainWDG);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);
        mainWDG->adjustSize();
        ChannelsWindow->setMinimumSize(mainWDG->size());
        // ChannelsWindow->adjustSize();

        retranslateUi(ChannelsWindow);

        QMetaObject::connectSlotsByName(ChannelsWindow);
    } // setupUi

    void retranslateUi(QDialog *ChannelsWindow)
    {
        ChannelsWindow->setWindowTitle(QCoreApplication::translate("Dialog", "MIDI Channels", nullptr));
        chNoLBL->setText(QCoreApplication::translate("Dialog", "Channel No.", nullptr));
        visibLBL->setText(QCoreApplication::translate("Dialog", "Visible", nullptr));
        chNameLBL->setText(QCoreApplication::translate("Dialog", "Channel name", nullptr));
        instrumentLBL->setText(QCoreApplication::translate("Dialog", "Instrument", nullptr));

    } // retranslateUi

};

namespace Ui3 {
    class MidiChannelsWindow: public Ui_ChannelsWindow {};
} // namespace Ui3

QT_END_NAMESPACE

#endif // MIDI_CHANNELSSZEOQB_H
