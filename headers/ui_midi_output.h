/********************************************************************************
** Form generated from reading UI file 'midi_outputNcaHVA.ui'
**
** Created by: Qt User Interface Compiler version 5.15.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef MIDI_OUTPUTNCAHVA_H
#define MIDI_OUTPUTNCAHVA_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

#include "QMidi/headers/QMidiOut.h"

QT_BEGIN_NAMESPACE

class Ui_MidiOutputWindow
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *mainlayoutVL;
    QList<QRadioButton*> midioutRBs;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *bottomHL;
    QPushButton *testBTN;
    QDialogButtonBox *standardBTNs;
    QMap<QString,QString> outputList;

    ~Ui_MidiOutputWindow()
    {
        if (mainlayoutVL != nullptr)
            delete mainlayoutVL;
    }

    void setupUi(QDialog *MidiOutputWindow)
    {
        QRadioButton *tempRB;
        if (MidiOutputWindow->objectName().isEmpty())
            MidiOutputWindow->setObjectName(QString::fromUtf8("MidiOutputWindow"));
        // MidiOutputWindow->resize(646, 385);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MidiOutputWindow->sizePolicy().hasHeightForWidth());
        MidiOutputWindow->setSizePolicy(sizePolicy);
        MidiOutputWindow->setMinimumSize(QSize(300, 200));
        gridLayout = new QGridLayout(MidiOutputWindow);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        mainlayoutVL = new QVBoxLayout();
        mainlayoutVL->setSpacing(0);
        mainlayoutVL->setObjectName(QString::fromUtf8("mainlayoutVL"));
        mainlayoutVL->setSizeConstraint(QLayout::SetMaximumSize);
        outputList = QMidiOut::devices();
        for (QString key : outputList.keys()) {
            QString value = outputList.value(key);
            tempRB = new QRadioButton(MidiOutputWindow);
            tempRB->setObjectName(QString("action"+ key));
            tempRB->setText(QString(value + " (" + key + ")"));
            midioutRBs.append(tempRB);
            mainlayoutVL->addWidget(tempRB, 0, Qt::AlignHCenter);
        }

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        mainlayoutVL->addItem(verticalSpacer);

        bottomHL = new QHBoxLayout();
        bottomHL->setObjectName(QString::fromUtf8("bottomHL"));
        testBTN = new QPushButton(MidiOutputWindow);
        testBTN->setObjectName(QString::fromUtf8("testBTN"));

        bottomHL->addWidget(testBTN);

        standardBTNs = new QDialogButtonBox(MidiOutputWindow);
        standardBTNs->setObjectName(QString::fromUtf8("standardBTNs"));
        standardBTNs->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        bottomHL->addWidget(standardBTNs);


        mainlayoutVL->addLayout(bottomHL);


        gridLayout->addLayout(mainlayoutVL, 0, 1, 1, 1);


        retranslateUi(MidiOutputWindow);

        QMetaObject::connectSlotsByName(MidiOutputWindow);
    } // setupUi

    void retranslateUi(QDialog *MidiOutputWindow)
    {
        MidiOutputWindow->setWindowTitle(QCoreApplication::translate("MidiOutputWindow", "Select MIDI output", nullptr));
        testBTN->setText(QCoreApplication::translate("MidiOutputWindow", "Test", nullptr));
    } // retranslateUi

};

namespace Ui2 {
class MidiOutputWindow: public Ui_MidiOutputWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // MIDI_OUTPUTNCAHVA_H
