/********************************************************************************
** Form generated from reading UI file 'main_windowHstDRI.ui'
**
** Created by: Qt User Interface Compiler version 5.15.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef MAIN_WINDOWHSTDRI_H
#define MAIN_WINDOWHSTDRI_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <QMidiOut.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QWidget *centralwidget;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *uppermostVL;
    QHBoxLayout *channelsHL;
    QGridLayout *channelGL;
    QProgressBar *channelsoundBAR;
    QSlider *channelpressureVS;
    QLabel *channelnameLBL;
    QLabel *channelnumberLBL;
    QPushButton *muteBTN;
    QPushButton *soloBTN;
    QComboBox *comboBox;
    QVBoxLayout *playbackVL;
    QHBoxLayout *tempovolumeHL;
    QLabel *tempoLBL;
    QSlider *tempoHL;
    QPushButton *resettempoBTN;
    QLabel *volumeLBL;
    QSlider *volumeHL;
    QPushButton *resetvolumeBTN;
    QHBoxLayout *timelineHL;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton;
    QSlider *horizontalSlider;
    QLabel *label;
    QMenuBar *menubar;
    QMenu *menuFile;
    QMenu *menuMIDI_Output;
    QStatusBar *statusbar;
    QMap<QString, QString> vals;

    void setupUi(QMainWindow *MainWindow)
    {
        QAction *tempaction;
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 600);
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));

//        actionDefault_MIDI_Output = new QAction(MainWindow);
//        actionDefault_MIDI_Output->setObjectName(QString::fromUtf8("actionDefault_MIDI_Output"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayoutWidget = new QWidget(centralwidget);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(9, 9, 781, 541));
        uppermostVL = new QVBoxLayout(verticalLayoutWidget);
        uppermostVL->setObjectName(QString::fromUtf8("uppermostVL"));
        uppermostVL->setContentsMargins(0, 0, 0, 0);
        channelsHL = new QHBoxLayout();
        channelsHL->setObjectName(QString::fromUtf8("channelsHL"));
        channelGL = new QGridLayout();
        channelGL->setObjectName(QString::fromUtf8("channelGL"));
        channelsoundBAR = new QProgressBar(verticalLayoutWidget);
        channelsoundBAR->setObjectName(QString::fromUtf8("channelsoundBAR"));
        channelsoundBAR->setMaximum(127);
        channelsoundBAR->setValue(0);
        channelsoundBAR->setOrientation(Qt::Vertical);

        channelGL->addWidget(channelsoundBAR, 3, 0, 1, 1, Qt::AlignHCenter);

        channelpressureVS = new QSlider(verticalLayoutWidget);
        channelpressureVS->setObjectName(QString::fromUtf8("channelpressureVS"));
        channelpressureVS->setAutoFillBackground(false);
        channelpressureVS->setMaximum(127);
        channelpressureVS->setValue(100);
        channelpressureVS->setOrientation(Qt::Vertical);

        channelGL->addWidget(channelpressureVS, 3, 1, 1, 1, Qt::AlignHCenter);

        channelnameLBL = new QLabel(verticalLayoutWidget);
        channelnameLBL->setObjectName(QString::fromUtf8("channelnameLBL"));

        channelGL->addWidget(channelnameLBL, 0, 1, 1, 1, Qt::AlignRight);

        channelnumberLBL = new QLabel(verticalLayoutWidget);
        channelnumberLBL->setObjectName(QString::fromUtf8("channelnumberLBL"));

        channelGL->addWidget(channelnumberLBL, 0, 0, 1, 1, Qt::AlignLeft);

        muteBTN = new QPushButton(verticalLayoutWidget);
        muteBTN->setObjectName(QString::fromUtf8("muteBTN"));
        muteBTN->setEnabled(false);
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(muteBTN->sizePolicy().hasHeightForWidth());
        muteBTN->setSizePolicy(sizePolicy);

        channelGL->addWidget(muteBTN, 2, 0, 1, 1, Qt::AlignHCenter);

        soloBTN = new QPushButton(verticalLayoutWidget);
        soloBTN->setObjectName(QString::fromUtf8("soloBTN"));
        soloBTN->setEnabled(false);
        sizePolicy.setHeightForWidth(soloBTN->sizePolicy().hasHeightForWidth());
        soloBTN->setSizePolicy(sizePolicy);

        channelGL->addWidget(soloBTN, 2, 1, 1, 1, Qt::AlignHCenter);

        comboBox = new QComboBox(verticalLayoutWidget);
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        channelGL->addWidget(comboBox, 1, 0, 1, 2);


        channelsHL->addLayout(channelGL);


        uppermostVL->addLayout(channelsHL);

        playbackVL = new QVBoxLayout();
        playbackVL->setObjectName(QString::fromUtf8("playbackVL"));
        tempovolumeHL = new QHBoxLayout();
        tempovolumeHL->setObjectName(QString::fromUtf8("tempovolumeHL"));
        tempoLBL = new QLabel(verticalLayoutWidget);
        tempoLBL->setObjectName(QString::fromUtf8("tempoLBL"));

        tempovolumeHL->addWidget(tempoLBL);

        tempoHL = new QSlider(verticalLayoutWidget);
        tempoHL->setObjectName(QString::fromUtf8("tempoHL"));
        tempoHL->setMinimum(10);
        tempoHL->setMaximum(200);
        tempoHL->setValue(100);
        tempoHL->setOrientation(Qt::Horizontal);

        tempovolumeHL->addWidget(tempoHL);

        resettempoBTN = new QPushButton(verticalLayoutWidget);
        resettempoBTN->setObjectName(QString::fromUtf8("resettempoBTN"));

        tempovolumeHL->addWidget(resettempoBTN);

        volumeLBL = new QLabel(verticalLayoutWidget);
        volumeLBL->setObjectName(QString::fromUtf8("volumeLBL"));

        tempovolumeHL->addWidget(volumeLBL);

        volumeHL = new QSlider(verticalLayoutWidget);
        volumeHL->setObjectName(QString::fromUtf8("volumeHL"));
        volumeHL->setMaximum(120);
        volumeHL->setValue(100);
        volumeHL->setOrientation(Qt::Horizontal);

        tempovolumeHL->addWidget(volumeHL);

        resetvolumeBTN = new QPushButton(verticalLayoutWidget);
        resetvolumeBTN->setObjectName(QString::fromUtf8("resetvolumeBTN"));

        tempovolumeHL->addWidget(resetvolumeBTN);


        playbackVL->addLayout(tempovolumeHL);

        timelineHL = new QHBoxLayout();
        timelineHL->setObjectName(QString::fromUtf8("timelineHL"));
        pushButton_2 = new QPushButton(verticalLayoutWidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        timelineHL->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(verticalLayoutWidget);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        timelineHL->addWidget(pushButton_3);

        pushButton = new QPushButton(verticalLayoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        timelineHL->addWidget(pushButton);

        horizontalSlider = new QSlider(verticalLayoutWidget);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setOrientation(Qt::Horizontal);

        timelineHL->addWidget(horizontalSlider);

        label = new QLabel(verticalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        timelineHL->addWidget(label);


        playbackVL->addLayout(timelineHL);


        uppermostVL->addLayout(playbackVL);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 22));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuMIDI_Output = new QMenu(menubar);
        menuMIDI_Output->setObjectName(QString::fromUtf8("menuMIDI_Output"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menuFile->menuAction());
        menubar->addAction(menuMIDI_Output->menuAction());
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave);
        vals = QMidiOut::devices();
        for (QString key : vals.keys()) {
            QString value = vals.value(key);
            tempaction = new QAction(MainWindow);
            tempaction->setObjectName(QString("action" + value + "_" + key));
            tempaction->setText(QString("action" + value + " (" + key + ")"));
            menuMIDI_Output->addAction(tempaction);
        }

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "Cute Midi Player", nullptr));
        actionOpen->setText(QCoreApplication::translate("MainWindow", "Open", nullptr));
#if QT_CONFIG(tooltip)
        actionOpen->setToolTip(QCoreApplication::translate("MainWindow", "Browse for .midi file", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        actionOpen->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+O", nullptr));
#endif // QT_CONFIG(shortcut)
        actionSave->setText(QCoreApplication::translate("MainWindow", "Save", nullptr));
#if QT_CONFIG(tooltip)
        actionSave->setToolTip(QCoreApplication::translate("MainWindow", "Save .midi file with defined channel levels", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        actionSave->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+S", nullptr));
#endif // QT_CONFIG(shortcut)
        channelsoundBAR->setFormat(QString());
        channelnameLBL->setText(QCoreApplication::translate("MainWindow", "Unnamed", nullptr));
        channelnumberLBL->setText(QCoreApplication::translate("MainWindow", "Ch1:", nullptr));
#if QT_CONFIG(tooltip)
        muteBTN->setToolTip(QCoreApplication::translate("MainWindow", "Mute channel", nullptr));
#endif // QT_CONFIG(tooltip)
        muteBTN->setText(QCoreApplication::translate("MainWindow", "M", nullptr));
        soloBTN->setText(QCoreApplication::translate("MainWindow", "S", nullptr));
        comboBox->setItemText(0, QCoreApplication::translate("MainWindow", "Voice Ooh", nullptr));
        comboBox->setItemText(1, QCoreApplication::translate("MainWindow", "Piano", nullptr));

        tempoLBL->setText(QCoreApplication::translate("MainWindow", "Tempo", nullptr));
#if QT_CONFIG(tooltip)
        resettempoBTN->setToolTip(QCoreApplication::translate("MainWindow", "Reset tempo to default value saved in .midi file", nullptr));
#endif // QT_CONFIG(tooltip)
        resettempoBTN->setText(QCoreApplication::translate("MainWindow", "Reset Tempo", nullptr));
        volumeLBL->setText(QCoreApplication::translate("MainWindow", "Volume", nullptr));
#if QT_CONFIG(tooltip)
        resetvolumeBTN->setToolTip(QCoreApplication::translate("MainWindow", "Reset volume to 100 %", nullptr));
#endif // QT_CONFIG(tooltip)
        resetvolumeBTN->setText(QCoreApplication::translate("MainWindow", "Reset Volume", nullptr));
#if QT_CONFIG(tooltip)
        pushButton_2->setToolTip(QCoreApplication::translate("MainWindow", "One bar backwards", nullptr));
#endif // QT_CONFIG(tooltip)
        pushButton_2->setText(QCoreApplication::translate("MainWindow", "Bwd", nullptr));
#if QT_CONFIG(tooltip)
        pushButton_3->setToolTip(QCoreApplication::translate("MainWindow", "Play .midi file", nullptr));
#endif // QT_CONFIG(tooltip)
        pushButton_3->setText(QCoreApplication::translate("MainWindow", "Play", nullptr));
#if QT_CONFIG(tooltip)
        pushButton->setToolTip(QCoreApplication::translate("MainWindow", "One bar forward", nullptr));
#endif // QT_CONFIG(tooltip)
        pushButton->setText(QCoreApplication::translate("MainWindow", "Fwd", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "0:00/0:00", nullptr));
        menuFile->setTitle(QCoreApplication::translate("MainWindow", "File", nullptr));
#if QT_CONFIG(tooltip)
        menuMIDI_Output->setToolTip(QCoreApplication::translate("MainWindow", "Select MIDI output device (port)", nullptr));
#endif // QT_CONFIG(tooltip)
        menuMIDI_Output->setTitle(QCoreApplication::translate("MainWindow", "MIDI Output", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // MAIN_WINDOWHSTDRI_H
