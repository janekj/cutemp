# Cute MIDI Player

Simple MIDI player with a mixer (channel volume balance), instrument change and the possibility to save rebalanced MIDI files, optionally with renamed channel names.

## Acknowledgement

This MIDI player is based on:

- (modified) [QMidi library](https://github.com/waddlesplash/QMidi)
- [Qt5 platform](https://www.qt.io/)
- [M Cross Environment](https://mxe.cc/) – used to cross-compile the Windows version
- [Standard MIDI file format description by Ichiro Fujinaga](https://www.music.mcgill.ca/~ich/classes/mumt306/StandardMIDIfileformat.html)

## Screenshots

![Main window – Ubuntu 22.04](screenshots/ubuntu22-main.png "The main window on Ubuntu 22.04 with the Adwaita-Dark theme")

![Edit channels – Ubuntu 22.04](screenshots/ubuntu22-channels.png "Edit channels on Ubuntu 22.04 with the Adwaita-Dark theme")

![Main window – Windows 10](screenshots/windows-main.png "The main window on Windows 10")

![Edit channels – Windows 10](screenshots/windows-channels.png "Edit channels on Windows 10")

## Features

- channel volume mixer
- tempo change
- MIDI instrument change
- channel name change (particularly useful for Lilypond-generated MIDI files)
- save the resulting MIDI file
- MIDI to text (for MIDI debugging, experimental and not complete)

## Installation

### Windows

Download the `.zip` archive in the [binaries](./binaries) folder and extract it somewhere (`Program Files` recommended). To run the program, just run the `cutemp.exe`.

### Ubuntu

*Note: Some MIDI sequencer is needed to play MIDI files. I recommend using [FluidSynth](https://www.fluidsynth.org/). It can be installed by the `install.sh` script below. However, if you use another MIDI sequencer, you may not need to install FluidSynth. In that case, modify the `.desktop` file before installation and skip the FluidSynth installation step during the installation process.*

Download the `.tar.xz` archive in the [binaries](./binaries) folder and extract it. To install the program, run the `install.sh` script inside the extracted folder (needs `sudo` permissions for certain operations). Missing Qt5 libraries will be installed. The binary is linked against system-wide Qt5 libraries.

To customize appearance, you can use standard Qt5 tools, such as `qt5ct`. The Cute MIDI Player comes with 3 sets of icons – default (given by the global Qt5 style), white (useful for dark themes) and black (for light themes). These can be activated by running `cutemp` with option `-i w` or `-i b`, respectively.

### Other Linux distro

Some parts of the Ubuntu installation can be useful. You can always use `qmake` to build the app from the source...

A debian package created automatically by `linuxdeqployqt` is also available in [binaries](./binaries) folder. It may be useful for someone, but I found it suboptimal.

Any contribution is welcome, especially if you can create native packages for various linux distributions.

### Mac

This app should work on Mac, but I have no resources to compile it and test it, any contribution is welcome...

## Usage

To play a song, just open the MIDI file by `File→Open` or `Ctrl+O`. MIDI output must be set. This is done automatically on Windows (with its own synthesizer) and Linux with FluidSynth installed. Otherwise, you must select MIDI output by `Edit→MIDI Output`. A dialog window appears with possible outputs. You can test them (play a scale on channel 0) and select one. 

In `Edit→Channels` you can toggle channels visibility and change channel names and instruments. These names are exported when saving a MIDI file.

## Long story

I write music sheets in [Lilypond](https://lilypond.org/) and other software. The easiest way to control the final score is to play the MIDI file and check it against the original source. Sometimes (actually quite often) it is advantageous to set different volumes for different instruments (channels). While this is readily available in Sibelius, the feature is lacking in Lilypond and deserves a general solution.

I like [Sweet MIDI Player](https://www.ronimusic.com/swmipl.htm), but there is no version for Linux (you can still use the Windows version and Wine) and, much more importantly, it is not open-source. As I did not find an open-source alternative at the time when I wanted to start writing this app, I decided to make my own, at least as a practice. Since I had some experience with Qt, I write it in Qt using the QMidi library. Consequently, this app works both on Linux and Windows (tested) and should also work on Mac (if someone could compile it, I would be glad).

Being inspired by Sweet MIDI Player and using Qt, I decided to name it Cute MIDI Player.
