# CuteMidiPlayer QMakefile

QT = core gui widgets
TEMPLATE = app
TARGET = cutemp
QTPLUGIN += qsvg

include(QMidi/QMidi.pri)

SOURCES += sources/main.cpp \
    sources/aboutwindow.cpp \
    sources/mainwindow.cpp \
    sources/midichannelswindow.cpp \
    sources/midiplayer.cpp \
    sources/midioutputwindow.cpp

HEADERS += \
    headers/aboutwindow.h \
    headers/mainwindow.h \
    headers/midichannelswindow.h \
    headers/midioutputwindow.h \
    headers/midiplayer.h \
    headers/ui_about.h \
    headers/ui_main_window.h \
    headers/ui_midi_channels.h \
    headers/ui_midi_output.h

DISTFILES += \
    $$PWD/icons/black/backward.svg \
    $$PWD/icons/black/forward.svg \
    $$PWD/icons/black/pause.svg \
    $$PWD/icons/black/play.svg \
    $$PWD/icons/black/stop.svg \
    $$PWD/icons/program_icon.svg \
    $$PWD/icons/white/backward.svg \
    $$PWD/icons/white/forward.svg \
    $$PWD/icons/white/pause.svg \
    $$PWD/icons/white/play.svg \
    $$PWD/icons/white/stop.svg \
    icons/program_icon.ico

RESOURCES += \
    $$PWD/icons/sources.qrc

win32 {
    RC_ICONS = $$PWD/icons/program_icon.ico
}
