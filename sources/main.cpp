#include "headers/mainwindow.h"
#include <cstring>

#include <QtWidgets/QApplication>

void printHelp();

int main(int argc, char *argv[])
{
    char port[100];
    char file[100];
    port[0] = '\0';
    file[0] = '\0';
    int theme = 0;
    int i;
    QApplication a(argc, argv);

        // read commandline arguments
        for (i = 1; i < argc; i++)
        {
            if (argv[i][0] == '-') // options
            {
                switch (argv[i][1])
                {
                case 'h': // help
                    printHelp();
                    return -1;
                case 'p': // client port (MIDI output)
                    if ((++i < argc) && (strlen(argv[i]) < 99))
                        strcpy(port, argv[i]);
                    else
                        std::cerr << "MIDI client and port must be specified after -p\n";
                    break;
                case 'i': // icon theme
                    if ((++i < argc) && (strchr(argv[i], 'w')))
                        theme = 2;
                    else if (strchr(argv[i], 'b'))
                        theme = 1;
                    else
                        theme = 0;
                    break;
                default: // unknown option
                    std::cerr << "Skipping unknown option: " << std::to_string(argv[i][1]) << "\n";
                    break;
                }
            }
            else // filename
            {
                if (strlen(argv[i]) < 99)
                    strcpy(file, argv[i]);
            }
        }
    MainWindow w(nullptr, file, port, theme);
    w.show();
    return a.exec();
}

void printHelp()
{
    std::cout << "CuteMIDIPlayer\n";
    std::cout << "==============\n";
    std::cout << "Qt and QMidi based MIDI player with mixer and rebalanced file saving\n";
    std::cout << "Usage cutemp [FILE] [-p CLIENT:PORT] [-i w|b]\n";
    std::cout << "Where:\n";
    std::cout << "    FILE        ... MIDI file to open (must be standard MIDI file format 0 (one track per file)\n";
    std::cout << "                    otherwise the behaviour is undefined\n";
    std::cout << "    CLIENT:PORT ... midi sequencer client:port\n";
    std::cout << "                    on a Linux system, use `aconnect -l` to list available values\n";
    std::cout << "    w           ... white icons (use for dark Qt themes\n";
    std::cout << "    b           ... black icons (use for light Qt themes\n";
}


