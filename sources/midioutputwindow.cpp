#include "headers/midioutputwindow.h"
#include "headers/ui_midi_output.h"
#include <iostream>
#include <chrono>
#include <thread>


MidiOutputWindow::MidiOutputWindow(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui2::MidiOutputWindow)
{
    ui->setupUi(this);
    QObject::connect(ui->standardBTNs, &QDialogButtonBox::accepted,
                     this, &MidiOutputWindow::slotAccept);
    QObject::connect(ui->standardBTNs, &QDialogButtonBox::rejected,
                     this,&MidiOutputWindow::slotCancell);
    QObject::connect(ui->testBTN, &QPushButton::clicked,
                     this, &MidiOutputWindow::slotTest);
}

MidiOutputWindow::~MidiOutputWindow()
{
    delete ui;
}

void MidiOutputWindow::slotAccept()
{
    QList<QRadioButton*>::ConstIterator it;
    QString key;
    for (it = ui->midioutRBs.begin(); it != ui->midioutRBs.end(); it++)
    {
        if ((*it)->isChecked())
        {
            key = (*it)->objectName();
            key.remove(0, 6);
            break;
        }
    }
    if (key.isEmpty())
    {
        return; // nothing happens and user is forced to select output
    }
    emit this->acceptSelection(key);
    this->close();
}

void MidiOutputWindow::slotCancell()
{
    this->close();
}

void MidiOutputWindow::slotTest()
{
    QList<QRadioButton*>::ConstIterator it;
    QMidiOut testmidiout;
    QMidiEvent *e;
    QString key;
    for (it = ui->midioutRBs.begin(); it != ui->midioutRBs.end(); it++)
    {
        if ((*it)->isChecked())
        {
            key = (*it)->objectName();
            key.remove(0, 6);
            break;
        }
    }
    if (key.isEmpty())
    {
        return; // nothing happens and user is forced to select output
    }
    testmidiout.connect(key);
    e = new QMidiEvent();
    for (int i = 60; i < 73; i++)
    {
        e->setType(QMidiEvent::NoteOn);
        e->setTrack(0);
        e->setVoice(0);
        e->setNote(i);
        e->setVelocity(90);
        testmidiout.sendMsg(e->message());
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
        e->setType(QMidiEvent::NoteOff);
        e->setTrack(0);
        e->setVoice(0);
        e->setNote(i);
        e->setVelocity(90);
        testmidiout.sendMsg(e->message());
    }
    for (int i = 0; i < 16; i++)
    {
        testmidiout.resetChannelControllers(i);
    }
    testmidiout.disconnect();

}



