#include "headers/midichannelswindow.h"
#include "headers/ui_midi_channels.h"

MidiChannelsWindow::MidiChannelsWindow(Ui::MainWindow *ui_main, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui3::MidiChannelsWindow), main_ui(ui_main)
{
    ui->setupUi(this);
    setValues();
    QObject::connect(ui->buttonBox, &QDialogButtonBox::accepted,
                     this, &MidiChannelsWindow::slotAccept);
    QObject::connect(ui->buttonBox, &QDialogButtonBox::rejected,
                     this,&MidiChannelsWindow::slotCancell);
}

MidiChannelsWindow::~MidiChannelsWindow()
{
    delete ui;
}

void MidiChannelsWindow::getValues() const
{
    if (main_ui == nullptr)
    {
        return;
    }
    int i;
    for (i = 0; i < 16; i++)
    {
        main_ui->channelList[i]->channelnameLBL->setText(ui->channelList[i]->chnameLE->text());
        main_ui->channelList[i]->setVisible(ui->channelList[i]->channelvisChB->isChecked());
        main_ui->channelList[i]->comboBox->setCurrentIndex(ui->channelList[i]->chinstrumentCB->currentIndex());
    }
}

void MidiChannelsWindow::setValues()
{
    int i;
    for (i = 0; i < 16; i++)
    {
        // ui->channelList[i]->chnumberLBL->setText(QString::number(i));
        ui->channelList[i]->chnameLE->setText(main_ui->channelList[i]->channelnameLBL->text());
        ui->channelList[i]->channelvisChB->setChecked(main_ui->channelList[i]->isEnabled());
        ui->channelList[i]->chinstrumentCB->setCurrentIndex(main_ui->channelList[i]->comboBox->currentIndex());
    }
}

void MidiChannelsWindow::slotAccept()
{
    getValues();
    this->close();
}

void MidiChannelsWindow::slotCancell()
{
    this->close();
}
