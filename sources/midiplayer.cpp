#include "headers/midiplayer.h"
#include <cmath>
#include <iostream>

MidiPlayer::MidiPlayer(QMidiFile* file, QMidiOut* out)
{
    midi_file = file;
    midi_out = out;
    orig_total_duration = midi_file->getTotalTime() * 1000; // time in ms
    time_line = new QTimeLine(orig_total_duration, this);
    time_line->setEasingCurve(QEasingCurve(QEasingCurve::Linear));
    time_line->setUpdateInterval((qint32) round(midi_file->timeFromTick(1) * 1000)); // update time in ms
    nextevent = 0;
    tempo_factor = 1;
    nexttime = round(midi_file->timeFromTick(midi_file->events().at(nextevent)->tick()) * 1000); // time in ms
    QObject::connect(time_line, &QTimeLine::valueChanged,
                     this, &MidiPlayer::slotTimeUpdate);
    QObject::connect(time_line, &QTimeLine::finished,
                     this, &MidiPlayer::slotFinished);
    preprocessEvents();
}

MidiPlayer::MidiPlayer()
{
    midi_file = nullptr;
    midi_out = nullptr;
    time_line = nullptr;
    tempo_factor = 1;
}

MidiPlayer::~MidiPlayer()
{
    if (isPlaying())
    {
        stop();
    }
    if (midi_file != nullptr)
    {
        delete midi_file;
    }
    if (midi_out != nullptr)
    {
        midi_out->disconnect();
        delete midi_out;
    }
    if (time_line != nullptr)
    {
        delete time_line;
    }
}

void MidiPlayer::play()
{
    if ((midi_file == nullptr) || (midi_out == nullptr) || (time_line == nullptr))
    {
        std::cerr << "ERROR: Cannot start player without file or output set!\n";
        return;
    }
    time_line->blockSignals(false);
    if (time_line->state() == QTimeLine::NotRunning)
    {
        time_line->start();
    }
    else
    {
        time_line->setPaused(false);
    }
}

void MidiPlayer::pause()
{
    time_line->setPaused(true);
    midi_out->stopAll();
    emit signalAllNotesOff();
}

void MidiPlayer::stop(bool blocktimeupdate)
{
    if ((time_line != nullptr) && (time_line->state() != QTimeLine::NotRunning))
    {
        time_line->stop();
        time_line->setCurrentTime(0);
    }
    if (midi_out != nullptr)
    {
        midi_out->stopAll();
    }

    if (!blocktimeupdate && (midi_file != nullptr))
    {
        emit signalTimeUpdated(0.0, QPair<int, int>(0, 0));
        nextevent = 0;
        nexttime = midi_file->timeFromTick(midi_file->events().at(0)->tick());
    }
    emit signalAllNotesOff();
}

void MidiPlayer::setMidiFile(QMidiFile *file)
{
    if (midi_file != nullptr)
    {
        delete midi_file;
    }
    if (time_line != nullptr)
    {
        delete time_line;
    }
    midi_file = file;
    orig_total_duration = midi_file->getTotalTime() * 1000; // total time in ms
    time_line = new QTimeLine(orig_total_duration, this);
    time_line->setEasingCurve(QEasingCurve(QEasingCurve::Linear));
    time_line->setUpdateInterval((qint32) round(midi_file->timeFromTick(1) * 1000)); // update time in ms
    // std::cerr << "MIDI timeFromTick(1) = " << midi_file->timeFromTick(1) * 1000 << " ms\n";
    nextevent = 0;
    nexttime = round(midi_file->timeFromTick(midi_file->events().at(nextevent)->tick()) * 1000 / tempo_factor); // time in ms
    QObject::connect(time_line, &QTimeLine::valueChanged,
                     this, &MidiPlayer::slotTimeUpdate);
    QObject::connect(time_line, &QTimeLine::finished,
                     this, &MidiPlayer::slotFinished);
    preprocessEvents();
}

void MidiPlayer::setMidiOut(QMidiOut *midioutput)
{
    if (midi_out != nullptr)
    {
        midi_out->disconnect();
        delete midi_out;
    }
    midi_out = midioutput;
}

bool MidiPlayer::isPlaying()
{
    if (time_line != nullptr)
    {
        return (time_line->state() == QTimeLine::State::Running);
    }
    else
    {
        return false;
    }
}

int MidiPlayer::printReadable(Ui_MainWindow *ui, QString filename)
{
    if (midi_file == nullptr)
    {
        return 1;
    }
    int i;
    for (i = 0; i < 16; i++)
    {
        midi_file->createControlChangeEvent(0, 0, i, 121, 0);
        midi_file->createChannelVolumeEvent(0, 0, i, ui->channelList[i]->channelvolumeVS->value());
        midi_file->createProgramChangeEvent(0, 0, i, ui->channelList[i]->comboBox->currentIndex());
        midi_file->createMetaEvent(0, 0, 3, ui->channelList[i]->channelnameLBL->text().toUtf8()); // track name (instrument name)
    }
    // sort
    midi_file->enableSort();
    midi_file->sort();
    midi_file->enableSort(false);

    QFile *out;
    out = new QFile(filename);

    if (out->exists()) {
        out->remove();
    }
    if ((filename == "") || !(out->open(QFile::WriteOnly))) {
        return false;
    }

    QList<QMidiEvent*> events = midi_file->events();
    for (QMidiEvent* e : events)
    {
        e->printEvent(out);
    }
    out->close();
    return 0;
}

void MidiPlayer::slotTimeUpdate(qreal curtime)
{
    if (time_line->state() != QTimeLine::Running)
    {
        return;
    }
    QMidiEvent* e;
    qint32 curtick = 0;
    while (nexttime <= (qint64) round(curtime * getTotalDuration()))
    {

        e = midi_file->events().at(nextevent);
        curtick = e->tick();

        if (e->type() == QMidiEvent::SysEx)
        {
            // TODO: sysex
        }
        else if (e->isNoteEvent())
        {
            if (mutedchannels.contains(e->voice()))
            {
                // do nothing
                // std::cerr << "Signal muted from voice: " << e->voice() << "\n";
            }
            else
            {
                if ((!solochannels.empty()) && (!solochannels.contains(e->voice())))
                {
                    e->setVelocity(e->velocity() * OTHERSLEVEL);
                }
                if (e->type() == QMidiEvent::NoteOn)
                {
                    emit signalNoteOn(e->voice(), e->velocity());
                    // std::cerr << "Signal note on emitted\n";
                }
                else if (e->type() == QMidiEvent::NoteOff)
                {
                    emit signalNoteOff(e->voice(), e->velocity());
                }
                qint32 message = e->message();
                midi_out->sendMsg(message);
            }
        }
        else
        {
            qint32 message = e->message();
            midi_out->sendMsg(message);
        }

        if (nextevent < (midi_file->events().size() - 1))
        {
            nexttime = round(midi_file->timeFromTick(midi_file->events().at(++nextevent)->tick()) * 1000 / tempo_factor); // next event time in ms
        }
        else
        {
            return;
        }
        // std::cerr << "After event sending: nexttime = " << nexttime << " ms, curtime = " << curtime * orig_total_duration << " ms\n";
    }
    QPair<int, int> barBeat = QPair<int, int>(-1, -1);
    if (curtick > 0)
    {
        barBeat = midi_file->barBeatFromTick(curtick);
    }
    emit signalTimeUpdated(curtime, barBeat);
    // std::cerr << "curtick, bar, beat: " << QString::number(curtick) << ", " <<
}

double MidiPlayer::getTotalDuration() const
{
    if (prepared())
    {
        return orig_total_duration/tempo_factor;
    }
    else
    {
        return -1.0;
    }
}

// original song duration access
int MidiPlayer::getOrigTotalDuration() const
{
    if (midi_file != nullptr)
        return orig_total_duration;
    else
        return 0;
}

void MidiPlayer::slotChannelVolumeChanged(int channel, int volume)
{
    if (midi_out != nullptr)
    {
        midi_out->setChannelVolume(channel, volume);
    }
}

void MidiPlayer::slotChannelProgramChanged(int channel, int value)
{
    if (midi_out != nullptr)
    {
        midi_out->setInstrument(channel, value);
    }
}

void MidiPlayer::slotTimeChanged(int promiles)
{
    if (promiles == 1000)
    {
        //        time_line->stop();
        //        midi_out->stopAll();
        //        emit signalAllNotesOff();
        //        time_line->blockSignals(false);
        //        stop(true);
        slotFinished();
        return;
    }
    bool timelineblock = time_line->blockSignals(true);
    bool was_playing = isPlaying();
    int new_tick;
    QPair<int, int> barBeat;
    if (time_line->state() != QTimeLine::NotRunning)
    {
        time_line->stop();
    }
    new_tick = midi_file->tickFromTime((float) promiles * orig_total_duration * 1e-6); // time must be in s, whereas orig_total_duration is in ms
    barBeat = midi_file->barBeatFromTick(new_tick);
    // std::cerr << "new_tick: " << new_tick << ", last event tick: " << midi_file->events().last()->tick() << "\n";
    for (int i = 0; i < midi_file->events().size(); i++)
    {
        if (midi_file->events().at(i)->tick() >= new_tick)
        {
            nextevent = ((i - 1) > 0)?(i - 1):0;
            break;
        }
    }
    nexttime = round(midi_file->timeFromTick(midi_file->events().at(nextevent)->tick()) * 1000 / tempo_factor);
    // std::cerr << "nexttime: " << nexttime << ", next event tick: " << midi_file->events().at(nextevent)->tick() << "\n";
    time_line->start();
    time_line->setCurrentTime((int) round(getTotalDuration() * promiles / 1000));
    time_line->setPaused(true);
    midi_out->stopAll();
    time_line->blockSignals(timelineblock);
    emit signalAllNotesOff();
    if (was_playing)
    {
        time_line->setPaused(false);
    }
    emit signalTimeChanged(promiles, barBeat);

}

bool MidiPlayer::prepared() const
{
    if ((midi_file != nullptr) && (midi_out != nullptr))
    {
        return true;
    }
    else
    {
        return false;
    }
}

void MidiPlayer::soloChannel(int channel)
{
    if (solochannels.contains(channel))
    {
        solochannels.remove(channel);
    }
    else
    {
        solochannels.insert(channel);
    }
}

void MidiPlayer::muteChannel(int channel)
{
    if (mutedchannels.contains(channel))
    {
        mutedchannels.remove(channel);
    }
    else
    {
        mutedchannels.insert(channel);
        if (midi_out != nullptr)
        {
            midi_out->stopAll(channel);
            emit signalNoteOff(channel, 60);
        }
    }
}

void MidiPlayer::slotTempoFactorUpdate(int factor)
{
    double old_factor = tempo_factor;
    tempo_factor = (double) factor/100.0;
    bool was_playing = isPlaying();
    double curr_time = time_line->currentTime();
    bool tlblock = time_line->blockSignals(true);
    // stop playing if playing
    if (time_line->state() != QTimeLine::NotRunning)
    {
        time_line->stop();
    }
    // recalculate times
    nexttime /= tempo_factor/old_factor;
    time_line->start();
    time_line->setDuration((int) round((double) orig_total_duration/tempo_factor));
    time_line->setCurrentTime((int) round((double) curr_time / tempo_factor * old_factor));
    time_line->setPaused(true);
    time_line->blockSignals(tlblock);

    if (was_playing)
    {
        time_line->setPaused(false);
    }
    else
    {
        midi_out->stopAll();
        emit signalAllNotesOff();
    }
}

void MidiPlayer::slotFinished()
{
    stop();
    emit signalStopped();
}

void MidiPlayer::preprocessEvents()
{
    QString text;
    QList<QMidiEvent*> events = midi_file->events();
    QMidiEvent* prev_event = events.at(0);
    for (QMidiEvent* e : events)
    {
        switch (e->type())
        {
        case QMidiEvent::NoteOn:
        case QMidiEvent::NoteOff:
            if (!activechannels.contains(e->voice()))
            {
                activechannels.insert(e->voice());
            }
            break;
        case QMidiEvent::KeyPressure:
            break;
        case QMidiEvent::ChannelPressure:
            break;
        case QMidiEvent::ProgramChange: // instrument change
            emit signalChannelProgram(e->voice(), e->number());
            midi_file->removeEvent(e);
            break;
        case QMidiEvent::PitchWheel:
            break;
        case QMidiEvent::ControlChange:
            switch (e->number())
            {
            case 7: // ChannelVolumeChange
                emit signalChannelVolume(e->voice(), e->value());
                midi_file->removeEvent(e);
                break;
            case 121: // ResetAllControlers
                midi_file->removeEvent(e);
                break;
            default:
                break;
            }
            // 10 Pan, ...
            break;
        case QMidiEvent::Meta: // text, channel prefix, track name, ...
            text = QString::fromUtf8(e->data().constData(), e->data().size());
            switch(e->number())
            {
            case 1: // text
                break;
            case 2: // copyright
                break;
            case 3: // track name
                if ((prev_event->voice() > -1) && (prev_event->voice() < 16))
                {
                    channelnames[prev_event->voice()] = text;
                    midi_file->removeEvent(e);
                }
                break;
            case 4: // instrument name
                if ((prev_event->voice() > -1) && (prev_event->voice() < 16) && (channelnames[prev_event->voice()].isEmpty()))
                {
                    channelnames[prev_event->voice()] = text;
                    midi_file->removeEvent(e);
                }
                break;
            case 5: // lyrics
                break;
            case 6: // marker
                break;
            default:
                break;
            }

        default: // SysEx and Invalid
            break;
        }
        prev_event = e;
    }
    for (int i = 0; i < 16; i++)
    {
        if (!channelnames[i].isEmpty())
        {
            emit signalChannelName(i, channelnames[i]);
        }
        if (activechannels.contains(i))
        {
            emit signalChannelVisibility(i, true);
        }
        else
        {
            emit signalChannelVisibility(i, false);
        }
    }

}

void MidiPlayer::resetControllers(QList<ChannelUI *> channelList)
{
    if (midi_out == nullptr)
        return;
    int i;
    for (i = 0; i < 16; i++)
    {
        midi_out->resetChannelControllers(i);
        midi_out->setChannelVolume(i, channelList.at(i)->channelvolumeVS->value());
        midi_out->setInstrument(i, channelList.at(i)->comboBox->currentIndex());
    }
}

void MidiPlayer::saveMidiFile(Ui_MainWindow *ui, QString filename)
{
    int i;
    for (i = 0; i < 16; i++)
    {
        midi_file->createControlChangeEvent(0, 0, i, 121, 0);
        midi_file->createChannelVolumeEvent(0, 0, i, ui->channelList[i]->channelvolumeVS->value());
        midi_file->createProgramChangeEvent(0, 0, i, ui->channelList[i]->comboBox->currentIndex());
        midi_file->createMetaEvent(0, 0, 3, ui->channelList[i]->channelnameLBL->text().toUtf8()); // track name (instrument name)
    }
    // set tempo
    midi_file->changeTempo((float) ui->tempoSL->value()/100.0);
    // sort
    midi_file->enableSort();
    midi_file->sort();
    midi_file->enableSort(false);

    // save
    midi_file->save(filename);
}

void MidiPlayer::moveByBar(int no_bars)
{
    qreal time = (qreal) time_line->currentTime() * tempo_factor; // current time in ms from timeline
    qint32 curtick = midi_file->tickFromTime(time*0.001); // time must be in s
    curtick += no_bars * midi_file->currBeatsPerBar(curtick) * midi_file->tickFromBeat(1.0);
    if ((curtick > midi_file->events().last()->tick()) || (curtick < 0))
        return;
    time = midi_file->timeFromTick(curtick); // returns time in s
    slotTimeChanged((qint32) round(time*1e6/orig_total_duration));
    QPair<int, int> barBeat = {-1, -1};
    barBeat = midi_file->barBeatFromTick(curtick);
    emit signalTimeUpdated(time*1000/orig_total_duration, barBeat);
}
