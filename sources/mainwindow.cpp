#include "headers/mainwindow.h"
#include "headers/ui_main_window.h"
#include <iostream>
#include <QtWidgets/QFileDialog>
#include <QCloseEvent>
#include "headers/midiplayer.h"
#include "headers/midioutputwindow.h"
#include "headers/midichannelswindow.h"
#include "headers/aboutwindow.h"
#include <cmath>
#include <string>


MainWindow::MainWindow(QWidget *parent, char *file, char *port, int th)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
//    registerFileType("MIDIequence",           // Document type name
//                            "MIDI sequence", // User readable file type name
//                            ".midi",                   // file extension
//                            0,                          // index of the icon to use for the files.
//                            false);                      // register for DDE events

    theme = th;
    ui->setupUi(this, theme);

    midiplayer = new MidiPlayer();
    disablePlaying(true);

    // file opening
    QObject::connect(ui->actionOpen, &QAction::triggered,
                     this, &MainWindow::slotOpenFile);
    // file saving
    QObject::connect(ui->actionSave, &QAction::triggered,
                     this, &MainWindow::slotSaveFile);
    QObject::connect(ui->actionSaveAsText, &QAction::triggered,
                     this, &MainWindow::slotSaveFileAsText);
    // MIDI output selection
    QObject::connect(ui->actionMIDI_Output, &QAction::triggered,
                     this, &MainWindow::slotSelectMIDIOut);
    // MIDI channels setup window
    QObject::connect(ui->actionChannels, &QAction::triggered,
                     this, &MainWindow::slotMidiChannelsSetup);
    // show about window
    QObject::connect(ui->actionAbout, &QAction::triggered,
                     this, &MainWindow::slotAbout);
    // play/pause
    QObject::connect(ui->playpauseBTN, &QPushButton::clicked,
                     this, &MainWindow::slotPlayPause);
    // stop
    QObject::connect(ui->stopBTN, &QPushButton::clicked,
                     this, &MainWindow::slotStop);
    QObject::connect(midiplayer, &MidiPlayer::signalStopped,
                     this, &MainWindow::slotStop);
    // fwd, bwd
    QObject::connect(ui->fwdBTN, &QPushButton::clicked,
                     this, &MainWindow::slotFwd);
    QObject::connect(ui->bwdBTN, &QPushButton::clicked,
                     this, &MainWindow::slotBwd);
    // update time on slider
    QObject::connect(midiplayer, &MidiPlayer::signalTimeUpdated,
                     this, &MainWindow::slotTimeUpdated);
    // update time by slider
    QObject::connect(midiplayer, &MidiPlayer::signalTimeChanged,
                     this, &MainWindow::slotTimeChanged);
    QObject::connect(ui->timelineSL, &QSlider::valueChanged,
                     midiplayer, &MidiPlayer::slotTimeChanged);
    // update tempo factor
    QObject::connect(ui->tempoSL, &QSlider::valueChanged,
                     midiplayer, &MidiPlayer::slotTempoFactorUpdate);
    QObject::connect(ui->tempoSL, &QSlider::valueChanged,
                     this, &MainWindow::slotTempoChanged);
    // tempo reset
    QObject::connect(ui->resettempoBTN, &QPushButton::clicked,
                     this, &MainWindow::slotTempoReset);
    // notes on and off
    QObject::connect(midiplayer, &MidiPlayer::signalNoteOn,
                     this, &MainWindow::slotNoteOn);
    QObject::connect(midiplayer, &MidiPlayer::signalNoteOff,
                     this, &MainWindow::slotNoteOff);
    // channel volume
    for (int i= 0; i < 16; i++)
    {
        QObject::connect(ui->channelList.at(i), &ChannelUI::signalVolumeChanged,
                         midiplayer, &MidiPlayer::slotChannelVolumeChanged);
    }
    QObject::connect(midiplayer, &MidiPlayer::signalChannelVolume,
                     this, &MainWindow::slotChannelVolume);
    // channel program
    for (int i= 0; i < 16; i++)
    {
        QObject::connect(ui->channelList.at(i), &ChannelUI::signalProgramChanged,
                         midiplayer, &MidiPlayer::slotChannelProgramChanged);
    }
    QObject::connect(midiplayer, &MidiPlayer::signalChannelProgram,
                     this, &MainWindow::slotChannelProgram);
    // channel mute
    for (int i= 0; i < 16; i++)
    {
        QObject::connect(ui->channelList.at(i), &ChannelUI::signalChannelMuted,
                         this, &MainWindow::slotChannelMute);
    }
    // channel solo
    for (int i= 0; i < 16; i++)
    {
        QObject::connect(ui->channelList.at(i), &ChannelUI::signalChannelSolo,
                         this, &MainWindow::slotChannelSolo);
    }
    // all notes off
    QObject::connect(midiplayer, &MidiPlayer::signalAllNotesOff,
                     this, &MainWindow::slotZeroSoundBars);
    // channel name
    QObject::connect(midiplayer, &MidiPlayer::signalChannelName,
                     this, &MainWindow::slotChannelName);
    // channel visibility
    QObject::connect(midiplayer, &MidiPlayer::signalChannelVisibility,
                     this, &MainWindow::slotChannelVisibility);

    if (strlen(file) > 1)
    {
        openFile(QString(file));
    }
    if (strlen(port) > 1)
    {
        slotSetMidiOutput(QString(port));
    }
    else
    {
        // MIDI output heuristic
        setMidiOutputHeuristic();
    }

}

MainWindow::~MainWindow()
{
    delete midiplayer;
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    midiplayer->stop();
    midiplayer->resetControllers(ui->channelList);
    event->accept();
}

void MainWindow::slotOpenFile()
{

    QString filename = QFileDialog::getOpenFileName(this, tr("Open Midi file"), "/home/janekj/Documents", tr("Midi Files (*.mid *.midi)"));
    // std::cerr << "After open file dialog, filename: " << filename.toStdString() << "\n";
    openFile(filename);
}

void MainWindow::openFile(QString filename)
{
    QMidiFile *midifile;
    midifile = new QMidiFile();
    if (midifile->load(filename))
    {
        midiplayer->setMidiFile(midifile);
        ui->statusMiddle->setText(filename);
        songduration = msToMMSS(midiplayer->getOrigTotalDuration());
        // std::cerr << "songduration: " << midiplayer->getOrigTotalDuration() << "converts to " << songduration.toStdString() << "\n";
        ui->timelineLBL->setText(QString("0:00/") + songduration);
        if (midiplayer->prepared())
        {
            disablePlaying(false);
        }
    }
    else
    {
        std::cerr << "File cannot be load!\n";
    }
}

void MainWindow::slotSaveFile()
{
    QString filename = QFileDialog::getSaveFileName(this, tr("Save Midi file"), "/home/janekj/Documents", "*.midi");
    if (!(filename.endsWith(QString(".midi")) || filename.endsWith((QString(".mid")))))
    {
        filename = filename + QString(".midi");
    }
    midiplayer->saveMidiFile(ui, filename);
}

void MainWindow::slotSaveFileAsText()
{
    QString filename = QFileDialog::getSaveFileName(this, tr("Save Midi file as text"), "/home/janekj/Documents", "*.txt");
    if (!filename.endsWith(QString(".txt")))
    {
        filename = filename + QString(".txt");
    }
    midiplayer->printReadable(ui, filename);

}

void MainWindow::slotSelectMIDIOut()
{
    MidiOutputWindow* midiwin = new MidiOutputWindow(this);
    QObject::connect(midiwin, &MidiOutputWindow::acceptSelection,
                     this, &MainWindow::slotSetMidiOutput);
    midiwin->exec();
}

void MainWindow::slotAbout()
{
    AboutWindow* aboutwin = new AboutWindow(this);
    aboutwin->show();
}



void MainWindow::slotMidiChannelsSetup()
{
    MidiChannelsWindow* chanwin = new MidiChannelsWindow(ui, this);
    chanwin->exec();
    for (int i = 0; i < 16; i++)
    {
        slotChannelVisibility(i, ui->channelList[i]->isEnabled());
    }
}


void MainWindow::slotSetMidiOutput(QString midikey)
{
    QMidiOut *midiout = new QMidiOut();
    if (midiout->connect(midikey))
    {
        midiplayer->setMidiOut(midiout);
        ui->statusLeft->setText(QString("MIDI output port: ") + midikey);
        if (midiplayer->prepared())
        {
            disablePlaying(false);
        }
    }
    else
    {
        std::cerr << "Error during MIDI output connection!\n";
    }
}

void MainWindow::slotPlayPause()
{
    if (!midiplayer->isPlaying())
    {
        midiplayer->play();
        switch (theme)
        {
        case 1: // black icons
            ui->playpauseBTN->setIcon(QIcon("../icons/black/pause.svg"));
            break;
        case 2: // white icons
            ui->playpauseBTN->setIcon(QIcon("../icons/white/pause.svg"));
            break;
        default: // default Qt icons
            ui->playpauseBTN->setIcon(this->style()->standardIcon(QStyle::SP_MediaPause));
            break;
        }
    }
    else
    {
        midiplayer->pause();
        switch (theme)
        {
        case 1: // black icons
            ui->playpauseBTN->setIcon(QIcon("../icons/black/play.svg"));
            break;
        case 2: // white icons
            ui->playpauseBTN->setIcon(QIcon("../icons/white/play.svg"));
            break;
        default: // default Qt icons
            ui->playpauseBTN->setIcon(this->style()->standardIcon(QStyle::SP_MediaPlay));
            break;
        }
    }
}

void MainWindow::slotStop()
{
    midiplayer->stop();
    switch (theme)
    {
    case 1: // black icons
        ui->playpauseBTN->setIcon(QIcon("../icons/black/play.svg"));
        break;
    case 2: // white icons
        ui->playpauseBTN->setIcon(QIcon("../icons/white/play.svg"));
        break;
    default: // default Qt icons
        ui->playpauseBTN->setIcon(this->style()->standardIcon(QStyle::SP_MediaPlay));
        break;
    }
}

void MainWindow::slotFwd()
{
    midiplayer->moveByBar(1);
}

void MainWindow::slotBwd()
{
    midiplayer->moveByBar(-1);
}

void MainWindow::slotTimeUpdated(qreal curtime, QPair<int, int> barBeat)
{
    ui->timelineSL->blockSignals(true); // must block valueChanged from slider in order not be disinterpreted as a user action
    ui->timelineSL->setValue((int) round(curtime*1000));
    ui->timelineSL->blockSignals(false); // unblock signals from timeline slider
    ui->timelineLBL->setText(msToMMSS(curtime * midiplayer->getOrigTotalDuration()) + QString("/") + songduration);
    if (barBeat.first > -1)
        ui->statusRight->setText(QString("Bar/Beat = ") + QString::number(barBeat.first) + QString("/") + QString::number(barBeat.second));
}

void MainWindow::slotNoteOn(int channel, int velocity)
{
    ui->channelList.at(channel)->slotNoteEvent(velocity, true);
}

void MainWindow::slotNoteOff(int channel, int velocity)
{
    ui->channelList.at(channel)->slotNoteEvent(velocity, false);
}

void MainWindow::slotZeroSoundBars()
{
    for (int i = 0; i < 16; i++)
    {
        ui->channelList.at(i)->channelsoundBAR->setValue(0);
    }
}

void MainWindow::disablePlaying(bool disable)
{
    ui->playpauseBTN->setDisabled(disable);
    ui->stopBTN->setDisabled(disable);
    ui->bwdBTN->setDisabled(disable);
    ui->fwdBTN->setDisabled(disable);
    ui->timelineSL->setDisabled(disable);
    ui->tempoSL->setDisabled(disable);
    ui->resettempoBTN->setDisabled(disable);

    if (!disable)
    {
        // reset controllers
        midiplayer->resetControllers(ui->channelList);
        // set channel instruments and volume
        for (int i = 0; i < 16; i++)
        {
            midiplayer->slotChannelVolumeChanged(i, ui->channelList[i]->channelvolumeVS->value());
            midiplayer->slotChannelProgramChanged(i, ui->channelList[i]->comboBox->currentIndex());
        }
    }
}

void MainWindow::slotChannelSolo(int channel)
{
    midiplayer->soloChannel(channel);
}

void MainWindow::slotChannelMute(int channel)
{
    midiplayer->muteChannel(channel);
}

void MainWindow::slotTempoReset()
{
    ui->tempoSL->setValue(100);
}

void MainWindow::slotTempoChanged(int value)
{
    ui->tempoSL->setToolTip(QString::number(value) + QString(" %"));
}

void MainWindow::slotChannelVolume(int channel, int volume)
{
    ui->channelList.at(channel)->channelvolumeVS->setValue(volume);
    midiplayer->slotChannelVolumeChanged(channel, volume);
}

void MainWindow::slotChannelProgram(int channel, int instrument)
{
    ui->channelList.at(channel)->comboBox->setCurrentIndex(instrument);
    ui->channelList.at(channel)->comboBox->setToolTip(ui->channelList.at(channel)->comboBox->currentText());
    // std::cerr << "setting instrument on channel " << channel << " to " << instrument << "\n";
    midiplayer->slotChannelProgramChanged(channel, instrument);
}

QString MainWindow::msToMMSS(int timeinms)
{
    int min, sec;
    sec = (int) round(timeinms/1000);
    min = (int) floor((double) sec/60);
    sec -= 60 * min;
    return QStringLiteral("%1:%2").arg(min, 2, 10, QChar('0')).arg(sec, 2, 10, QChar('0'));
}

void MainWindow::slotTimeChanged(int value, QPair<int, int> barBeat)
{
    ui->timelineLBL->setText(msToMMSS((int) round((double) value / 1000 * midiplayer->getOrigTotalDuration())) + QString("/") + songduration);
    if (barBeat.first > -1)
        ui->statusRight->setText(QString("Bar/Beat = ") + QString::number(barBeat.first) + QString("/") + QString::number(barBeat.second));
}

void MainWindow::slotChannelVisibility(int channel, bool visible)
{
    ui->channelList[channel]->setVisible(visible);
    ui->channelsHL->setStretch(channel, visible?1:0);

}

void MainWindow::slotChannelName(int channel, QString name)
{
    ui->channelList[channel]->changeName(name);
}

void MainWindow::setMidiOutputHeuristic()
{
    QMap<QString, QString> outmap = QMidiOut::devices();
    if (outmap.size() == 1)
    {
        slotSetMidiOutput(outmap.begin().key());
        std::cerr << "The only found output connnected\n";
    }
    else
    {
        for (auto key: outmap.keys())
        {
            if (outmap.value(key).toStdString().find("FLUID") != std::string::npos)
            {
                slotSetMidiOutput(key);
                std::cerr << "FLUID synth output connected\n";
            }
        }
    }

}

