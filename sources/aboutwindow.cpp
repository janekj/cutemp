#include "headers/aboutwindow.h"

AboutWindow::AboutWindow(QWidget *parent)
    : QDialog{parent},
      ui(new Ui4::AboutWindow)
{
    ui->setupUi(this);
}

AboutWindow::~AboutWindow()
{
    delete ui;
}
