#!/bin/sh

# Installation script for CuteMIDIPlayer on Ubuntu
# (may be useful for other Unix systems also)
echo "Welcome to the installation guide for Cute MIDI Player by JJ"
echo "============================================================"

install_fluidsynth=1
create_desktop=1

# Paths
read -p "Choose the path for the installation (default '/usr/share/cutemp', path before '/cutemp' must exist):" install_path

if [ -z "$install_path" ]
then 
install_path="/usr/share/cutemp"
fi

echo "$install_path"

# FluidSynth installation
read -p "Do you want to install FluidSynth MIDI synthetizer (recommended, default yes)[y/n]:" fluids

if [ "$fluids" = 'n' ] || [ "$fluids" = 'N' ]
then
echo "FluidSynth installation will be skipped"
install_fluidsynth=0
else
echo "FluidSynth will be installed"
fi

# Desktop file
read -p "Do you want to create a desktop file (Cute MIDI Player will be available through Activities, recommended, default yes)[y/n]:" desktop

if [ "$desktop" = 'n' ] || [ "$desktop" = 'N' ]
then
echo "Desktop file will NOT be created"
create_desktop=0
else
read -p "Choose destination for the desktop file (default /usr/share/applications):" desktop_path
if [ -z "$desktop_path" ]
then
desktop_path="/usr/share/applications"
fi 
fi

read -p "Choose the colour of button icons (default – colored, white (useful for dark qt5 themes), black (recommended))[d/w/b]:" icon_color

echo '#!/bin/bash' > cutemp.sh
if [ "$install_fluidsynth" -gt 0 ]
then
echo "fluidsynth /usr/share/sounds/sf2/FluidR3_GM.sf2 -s -i 1>/dev/null 2>&1 &" >> cutemp.sh
else
read -p "If you want to start custom MIDI sequencer before starting Cute MIDI Player, give the command to start it:" command
echo "$command" >> cutemp.sh
fi

echo "sleep 1 1>/dev/null" >> cutemp.sh
echo "cd $install_path/bin" >> cutemp.sh

if [ "$icon_color" = "w" ] 
then
echo './cutemp -i w ${@}' >> cutemp.sh
elif [ "$icon_color" = 'b' ]
then 
echo './cutemp -i b ${@}' >> cutemp.sh
else
echo './cutemp ${@}' >> cutemp.sh
fi

mv cutemp.sh bin/

# Libraries (packages needed)
sudo apt install libqt5core5a libqt5gui5 libqt5widgets5 -y

if [ "$install_fluidsynth" -gt 0 ]
then
sudo apt install fluidsynth -y
fi

# Copy files (script, program, desktop, icons)
sudo mkdir "$install_path"
sudo mkdir "$install_path/bin"
sudo mkdir "$install_path/icons"
sudo cp -r icons/* "$install_path/icons/"
sudo cp bin/cutemp "$install_path/bin/"
sed -i 's|INSTALL_PATH|'"$install_path"'|g' bin/cutemp.sh
sudo cp bin/cutemp.sh "$install_path/bin/"
old=$PWD
cd "$install_path/bin"
sudo chmod +x *
cd "$old"
sudo ln -s "$install_path/bin/cutemp.sh" "/usr/bin/cutemp"

echo "echo \"Uninstalling Cute MIDI Player\"" > uninstall.sh
echo "sudo rm -rf \"$install_path/\"*" >> uninstall.sh
echo "sudo rmdir \"$install_path\"" >> uninstall.sh
echo "sudo rm /usr/bin/cutemp" >> uninstall.sh


if [ "$create_desktop" -gt 0 ]
then
sed -i 's|INSTALL_PATH|'"$install_path"'|g' cutemp.desktop
sudo cp cutemp.desktop "$desktop_path/cutemp.desktop"
echo "sudo rm \"$desktop_path/cutemp.desktop\"" >> uninstall.sh
fi

if [ "$install_fluidsynth" -gt 0 ]
then
cat <<EOT>> uninstall.sh
read -p "Do you want to uninstall FluidSynth (default yes)?[y/n]:" uf
if [ "$uf" = 'n' ] || [ "$uf" = 'N' ] 
then 
sudo apt autoremove fluidsynth
fi
EOT
fi

# suggest to autoremove dependencies in uninstall.sh
echo 'echo "Maybe you will want to uninstall qt5 dependencies by: \"sudo apt autoremove libqt5core5a libqt5gui5 libqt5widgets5\" but usually it is not a good idea..."' >> uninstall.sh

echo "echo \"Cute MIDI Player uninstalled\"" >> uninstall.sh

# sudo chown "$USER" uninstall.sh
chmod +xrw uninstall.sh

echo "Installation successfull!"
echo "To uninstall Cute MIDI Player, use uninstall.sh"
echo "Feedback welcome! See https://gitlab.com/janekj/cutemp."


# Quit
